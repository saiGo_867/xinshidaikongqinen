import React, { Component } from 'react';
import { View,Image,StyleSheet,AsyncStorage,TouchableOpacity,TouchableHighlight,Text,TextInput,Button,Platform } from 'react-native'
import { connect } from 'react-redux';
import commonVariable from '../commonVariable'
import MyHud from '../Utils/MyHud'
import NetUtil from '../Utils/NetUtil'
import LoadingHud from '../Utils/LoadingHud'
import ServerData from '../ServerData'
class Login extends Component {

  constructor(props) {
    super(props)
    this.account = ''
    this.password = ''
    this.state = {isPasswordHidden:true,reloadView:false}
  }

  componentDidMount(){
      AsyncStorage.getItem('loginData',(error,result)=>{
            if (!error) {
              let data = JSON.parse(result)
              if (result === null) { return }
              this.account = data.account
              this.password = data.password
              var serverData = new ServerData()
              if (serverData.userMessage !== null && serverData.userMessage !== undefined) {
                  this.setState({reloadView:true})
                  return
              }
              this.setState({reloadView:true},this.login)
            }
      })
  }

  login(){
      var serverData = new ServerData()
      if (/^1[3|4|5|7|8][0-9]{9}$/.test(this.account) === false) {
        // alert("请输入正确的帐号!")
        this.refs.myhud.showWithInfo('请输入正确的帐号!')
        return
      }
      if (this.password.length < 6) {
        // Alert.alert('密码至少为6位!','',[{text: '确定'}])
        this.refs.myhud.showWithInfo('密码至少为6位!')
        return
      }
      var paras = {'upass':this.password , 'umobile':this.account}
      this.refs.loadinghud.show()
      NetUtil.get('MobileLogin',paras,
        //成功回调
        (response) => {
            this.refs.loadinghud.dismiss()
            // let array = new JSONArray()
            // var strJSON = "{cid:'json cid'}";
            // var obj = new Function("return" + strJSON)();//转换后的JSON对象
            // alert(obj.cid);
            // alert(obj.constructor);
            // var appdelegate = NativeModules.Appdeleagate
            if (response['0'] === undefined) {
              //  Alert.alert('请检查你的帐号和密码!','',[{text: '确定'}])
               this.refs.myhud.showWithInfo('请检查你的帐号和密码!')
               return
            }

            let loginData = { account:this.account, password:this.password }
            AsyncStorage.setItem('loginData',JSON.stringify(loginData),(error)=>{})

            serverData.userMessage = response['0']

            this.props.navigation.navigate('Main')
            // Platform.OS === 'ios' ? NativeModules.RNNotificationManager.postNotification('pushNative',{'userMessage':response[0]}) : null
        },
        //失败回调
        (error) => {
          console.log(error);
           this.refs.loadinghud.dismiss()
          //  Alert.alert('请检查你的网络!','',[{text: '确定'}])
           this.refs.myhud.showWithInfo('请检查你的网络!')
        })
  }

  render(){
    this.rightImageUri = this.state.isPasswordHidden ? require('../img/2登录页/闭眼.png') : require('../img/2登录页/睁眼.png');
    return(
      <View style={styles.container}>
            <Image style={{marginTop:commonVariable.scaleVectical(260)}} source={require('../img/2登录页/logo.png')}/>
            <View style={[styles.input,{marginTop:commonVariable.scaleVectical(90)}]}>
                  <Image style={styles.imageLeft} source={require('../img/2登录页/用户.png')}/>
                  <TextInput style={styles.textInput} ref='accountInput' placeholder='请输入用户名' placeholderTextColor={textPlaceholderColor} selectionColor={textColor}
                             defaultValue = {this.account} keyboardType='numeric' underlineColorAndroid = 'transparent' onChangeText={(text)=> this.account=text}/>
                  <TouchableHighlight style={styles.imageRight}  onPress={()=>this.refs.accountInput.clear()}>
                        <Image source={require('../img/2登录页/X.png')}/>
                  </TouchableHighlight>
            </View>
            <View style={[styles.input,{marginTop:commonVariable.scaleVectical(60)}]}>
                  <Image style={styles.imageLeft} source={require('../img/2登录页/密码.png')}/>
                  <TextInput style={styles.textInput} ref='passwordInput' placeholder='请输入密码' placeholderTextColor={textPlaceholderColor} selectionColor={textColor}
                             defaultValue = {this.password} secureTextEntry={this.state.isPasswordHidden} underlineColorAndroid = 'transparent' onChangeText={(text)=>this.password=text}/>
                  <TouchableHighlight style={styles.imageRight}  onPress={()=>this.setState({ isPasswordHidden: !this.state.isPasswordHidden })}>
                        <Image source={this.rightImageUri}/>
                  </TouchableHighlight>
            </View>
            <TouchableHighlight style={[styles.button,{backgroundColor:themeOrange}]} underlayColor='#A04317' onPress={()=>this.login()}>
                  <Text style={styles.text}>登录</Text>
            </TouchableHighlight>
            <TouchableHighlight style={[styles.button,{backgroundColor:inputBorderColor}]} underlayColor='#242A35' onPress={()=>this.props.navigation.navigate('Register')}>
                  <Text style={styles.text}>注册</Text>
            </TouchableHighlight>
            <Text style={[styles.text,{marginTop:commonVariable.scaleVectical(60)}]} onPress={()=>this.props.navigation.navigate('ChangePassword')}>忘记密码?</Text>
            <MyHud ref='myhud'/>
            <LoadingHud ref='loadinghud'/>
      </View>
    )
  }
}

var styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center',
        backgroundColor:themeBackgroundColor
    },

    input:{
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'center',
      width:ScreenWidth-commonVariable.scaleHorizon(120),
      height:commonVariable.scaleVectical(130),
      borderColor:inputBorderColor,
      borderWidth:1,
      borderRadius:commonVariable.scaleVectical(130)*0.5,
    },

    imageLeft:{
      marginLeft:commonVariable.scaleHorizon(50),
      marginRight:commonVariable.scaleHorizon(50),
    },

    imageRight:{
      marginLeft:commonVariable.scaleHorizon(40),
      marginRight:commonVariable.scaleHorizon(40),
    },

    textInput:{
      flex: 1,
      textAlignVertical: 'center',
      color:textColor,
      fontSize:Platform.OS === 'ios' ? commonVariable.scaleHorizon(50) : commonVariable.scaleHorizon(40),
    },

    button:{
      alignItems:'center',
      justifyContent:'center',
      width:ScreenWidth-commonVariable.scaleHorizon(120),
      height:commonVariable.scaleVectical(130),
      borderRadius:commonVariable.scaleVectical(130)*0.5,
      marginTop:commonVariable.scaleVectical(60)
    },

    text:{
      color:textColor,
      fontSize:commonVariable.scaleHorizon(55)
    }
})
export default connect(state => ({}), dispatch => ({}))(Login);
