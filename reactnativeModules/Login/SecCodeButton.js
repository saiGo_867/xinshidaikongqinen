import React, { Component } from 'react';
import { View,Image,StyleSheet,TouchableHighlight,Text } from 'react-native'
import commonVariable from './commonVariable'
import NetUtil from '../NetUtil'
export default class SecCodeButton extends Component {

    constructor(){
        //倒计时属性与状态
        this.state = {
          timerCount: 5,
          timerTitle: "获取验证码",
          selfEnable: false
        }
    }

    componentWillUnmount(){
        this.interval && clearInterval(this.interval)
    }

    getSecCode(){
      // console.log(this.mobilePhone)
      // if(this.selfEnable === false){
      //    return
      // }
      if (/^1[3|4|5|7|8][0-9]{9}$/.test(this.mobilePhone) === false) {
        Alert.alert('请输入正确的手机号码!','',[{text: '确定'}])
        return
      }

      NetUtil.receivePasswordMessage(this.mobilePhone,this.secCode,
        //成功回调
        (response) => {
          console.log("======"+response)
      })
      this.setState({selfEnable: true})
      this.interval = setInterval(()=>{
          const timer = this.state.timerCount - 1
          if(timer===-2){
            this.interval&&clearInterval(this.interval)
            this.setState({
              timerCount: 5,
              timerTitle: "获取验证码",
              selfEnable: false
            })
          }else{
            this.setState({
              timerCount:timer,
              timerTitle: '重新获取('+this.state.timerCount+'s)'
            })
          }
      },1000)
    }


    render(){
        return(
            <TouchableHighlight disabled={this.state.selfEnable} style={styles.secCodeBtn} onPress={()=>this.getSecCode()}>
                <Text adjustsFontSizeToFit={true} style={{fontSize:commonVariable.scaleHorizon(40), color:themeBackgroundColor, textAlign:'center'}}>{this.state.timerTitle}</Text>
            </TouchableHighlight>
        )
    }
}

var styles = StyleSheet.create({
    secCodeBtn:{
        justifyContent:'center',
        backgroundColor:'#FFF',
        height:commonVariable.scaleHorizon(90),
        width:commonVariable.scaleHorizon(256),
        paddingLeft:commonVariable.scaleHorizon(10),
        paddingRight:commonVariable.scaleHorizon(10),
        marginRight:commonVariable.scaleHorizon(20),
        borderRadius:commonVariable.scaleHorizon(90)*0.5,
    }
})
