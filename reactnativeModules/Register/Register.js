import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View,Image,StyleSheet,AsyncStorage,TouchableOpacity,TouchableHighlight,Text,TextInput,Button,Alert } from 'react-native'
import commonVariable from '../commonVariable'
import LoadingHud from '../Utils/LoadingHud'
import MyHud from '../Utils/MyHud'
import NetUtil from '../Utils/NetUtil'
// import SecCodeButton from './SecCodeButton'
class Register extends Component {

  static navigationOptions = ({navigation})=> ({
      headerTitle:'帐号注册',
      headerLeft:<TouchableHighlight onPress={()=>navigation.goBack()}>
                      <Image style={{left:commonVariable.scaleHorizon(40)}} source={require('../img/3注册页/返回键.png')}/>
                 </TouchableHighlight>
  })

  constructor(props) {
    super(props)
    this.secCode = this.randomNumberInRange(1000,10000)
    this.confirmSecCode = ''
    this.mobilePhone=''
    this.confirmMobilePhone=''
    this.password=''

    //倒计时属性与状态
    this.state = {
      timerCount: 5,
      timerTitle: "获取验证码",
      selfEnable: false
    }
  }

  componentWillUnmount(){
      this.interval && clearInterval(this.interval)
  }

  getSecCode(){
    // console.log(this.mobilePhone)
    // if(this.selfEnable === false){
    //    return
    // }

    if (/^1[3|4|5|7|8][0-9]{9}$/.test(this.mobilePhone) === false) {
      Alert.alert('请输入正确的手机号码!','',[{text: '确定'}])
      return
    }

    NetUtil.receivePasswordMessage(this.mobilePhone,this.secCode,
      //成功回调
      (response) => {
        console.log("======"+response)
    })
    this.confirmMobilePhone = this.mobilePhone
    this.setState({selfEnable: true})
    this.interval = setInterval(()=>{
        const timer = this.state.timerCount - 1
        if(timer===-2){
          this.interval&&clearInterval(this.interval)
          this.setState({
            timerCount: 5,
            timerTitle: "获取验证码",
            selfEnable: false
          })
        }else{
          this.setState({
            timerCount:timer,
            timerTitle: '重新获取('+this.state.timerCount+'s)'
          })
        }
    },1000)
  }

  register(){
      if (this.password === undefined || this.password.length<6){
        Alert.alert('密码至少为6位!','',[{text: '确定'}])
        return
      }
      if (/^1[3|4|5|7|8][0-9]{9}$/.test(this.mobilePhone) === false) {
        Alert.alert('请输入正确的手机号码!','',[{text: '确定'}])
        return
      }
      if (this.mobilePhone !== this.confirmMobilePhone){
        Alert.alert('手机号码不一致!','',[{text: '确定'}])
        return
      }
      if (this.secCode != this.confirmSecCode){
        Alert.alert('验证码不正确!','',[{text: '确定'}])
        return
      }
      var paras = {'uname':this.mobilePhone,'upass':this.password,'umobile':this.mobilePhone}
      this.refs.loadinghud.show()
      NetUtil.get('Regist',paras,
          //成功回调
          (response) => {
              this.refs.loadinghud.dismiss()
              if(response===1){
                  Alert.alert('此手机号码已注册!','',[{text: '确定'}])
                  return
              }
              Alert.alert('注册成功!','',[{text: '确定',onPress:()=>{
                  // this.props.navigation.state.params.callback(this.mobilePhone)
                  this.props.navigation.goBack()
              }
            }])
          },
          //失败回调
          (error) => {
              this.refs.loadinghud.dismiss()
              Alert.alert('请检查你的网络!','',[{text: '确定'}])
      })
  }

  randomNumberInRange(lowerLimit, upperLimit) {
      return Math.floor(Math.random() * (1 + upperLimit - lowerLimit)) + lowerLimit;
  }

  render(){
    return(
      <View style={styles.container}>
            <View style={{height:0.5,backgroundColor:inputBorderColor}}/>
            <View style={styles.row}>
                  <Text style={styles.previousText}>手机号码:</Text>
                  <TextInput style={styles.textInput} placeholder='手机号码将作为唯一帐号' placeholderTextColor={textPlaceholderColor}
                             onChangeText={(text)=> this.mobilePhone=text} selectionColor={textColor} underlineColorAndroid = 'transparent'/>
            </View>
            <View style={styles.row}>
                  <Text style={styles.previousText}>登录密码:</Text>
                  <TextInput style={styles.textInput} placeholder='6-16个字符以内' placeholderTextColor={textPlaceholderColor}
                             onChangeText={(text)=> this.password=text} selectionColor={textColor} secureTextEntry={true} underlineColorAndroid = 'transparent'/>
            </View>
            <View style={styles.row}>
                  <Text style={styles.previousText}>验 证 码 :</Text>
                  <TextInput style={styles.textInput} placeholder='手机验证码' placeholderTextColor={textPlaceholderColor}
                             onChangeText={(text)=> this.confirmSecCode=text} selectionColor={textColor} underlineColorAndroid = 'transparent'/>
                  <TouchableHighlight disabled={this.state.selfEnable} style={styles.secCodeBtn} onPress={()=>this.getSecCode()}>
                      <Text adjustsFontSizeToFit={true} numberOfLines={1}
                            style={{fontSize:commonVariable.scaleHorizon(40), color:themeBackgroundColor, textAlign:'center'}}>{this.state.timerTitle}</Text>
                  </TouchableHighlight>
            </View>
            <TouchableHighlight style={styles.button} underlayColor='#A04317' onPress={()=>this.register()}>
                  <Text style={styles.text}>注册</Text>
            </TouchableHighlight>
            <Text style={{marginTop:commonVariable.scaleVectical(60),alignSelf:'center',
                          color:textColor,fontSize:commonVariable.scaleHorizon(45)}}>注册即代表同意新时代协议>></Text>
            <MyHud ref='myhud' clickOn={()=> this.props.navigation.goBack()}/>
            <LoadingHud ref='loadinghud'/>
      </View>
    )
  }
}

var styles = StyleSheet.create({
  container:{
      flex:1,
      backgroundColor:themeBackgroundColor,
  },
  row:{
      flexDirection:'row',
      alignItems:'center',
      marginTop:commonVariable.scaleVectical(60),
      marginLeft:commonVariable.scaleVectical(60),
      marginRight:commonVariable.scaleVectical(60),
      height:commonVariable.scaleVectical(130),
      borderColor:inputBorderColor,
      borderWidth:1,
      borderRadius:commonVariable.scaleVectical(130)*0.5,
  },
  previousText:{
      color:textColor,
      textAlign:'justify',
      marginLeft:commonVariable.scaleHorizon(50),
      fontSize:commonVariable.scaleHorizon(45)
  },
  textInput:{
      flex:1,
      color:textColor,
      marginLeft:commonVariable.scaleHorizon(50),
      fontSize:commonVariable.scaleHorizon(45),
  },
  button:{
    alignItems:'center',
    justifyContent:'center',
    marginTop:commonVariable.scaleVectical(60),
    marginLeft:commonVariable.scaleVectical(60),
    marginRight:commonVariable.scaleVectical(60),
    height:commonVariable.scaleVectical(130),
    borderRadius:commonVariable.scaleVectical(130)*0.5,
    backgroundColor:themeOrange
  },
  text:{
    color:textColor,
    fontSize:commonVariable.scaleHorizon(55)
  },
  secCodeBtn:{
      justifyContent:'center',
      backgroundColor:'#FFF',
      height:commonVariable.scaleHorizon(90),
      width:85,
      paddingLeft:commonVariable.scaleHorizon(10),
      paddingRight:commonVariable.scaleHorizon(10),
      marginRight:commonVariable.scaleHorizon(20),
      borderRadius:commonVariable.scaleHorizon(90)*0.5,
  }
})
export default connect(state => ({
    routes: state.nav.routes
}), dispatch => ({}))(Register);
