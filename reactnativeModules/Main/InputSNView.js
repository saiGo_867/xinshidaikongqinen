import React, { Component } from 'react'
import { Animated,View,Alert,StyleSheet,TextInput,TouchableHighlight,Text,Easing,NativeModules } from 'react-native'
import { connect } from 'react-redux';
import store from '../store'
export default  class InputSNView extends Component{
    constructor(props){
        super(props)
        this.sn = ''
        this.state = {
          fadeOutOpacity: new Animated.Value(0),
          isShow: false
        }
    }

    // componentDidMount(){
    //   console.log(`${this.props}332323`);
    // }

    show(){
      this.setState({ isShow:true },()=>{
          Animated.timing(this.state.fadeOutOpacity, {
              toValue: 1,
              duration: 500,
              easing: Easing.linear,// 线性的渐变函数
          }).start();
      })
    }

    dismiss(){
      Animated.timing(this.state.fadeOutOpacity, {
          toValue: 0,
          duration: 500,
          easing: Easing.linear,// 线性的渐变函数
      }).start(()=>{
          this.setState({isShow:false})
      });
    }

    commit(){
        if (this.sn.length < 8) {
            Alert.alert('SN号不能小于8位!')
            return
        }
        NativeModules.RNHelper.mqttSubscribe(this.sn)
        store.dispatch({
            type: 'SUBSCRIBENEWSN',
            payload: this.sn
        });
        this.dismiss()
    }

    render(){
      if(this.state.isShow === true){
        return(
          <Animated.View style={[styles.container,{opacity: this.state.fadeOutOpacity}]}>
              <View style={styles.content}>
                  <Text style={{color:textColor,backgroundColor:themeOrange,textAlign:'center',
                                fontSize:18,paddingTop:10,textAlignVertical:'center',height:38,width:'100%'}}>添加设备</Text>
                  <View style={{backgroundColor:'#FFF',flex:1,marginLeft:17,marginRight:17,paddingTop:10,borderBottomWidth:0.5,borderColor:inputBorderColor}}>
                      <TextInput ref='SNInput' style={{flex:1,color:textPlaceholderColor,fontSize:16}}  placeholderTextColor={textPlaceholderColor}
                                 onChangeText={(text)=> this.sn=text} placeholder='请填写产品序列号...'/>
                  </View>
                  <View style={{backgroundColor:'#FFF',flex:1,flexDirection:'row'}}>
                        <Text style={{flex:1,color:textPlaceholderColor,textAlign:'center',fontSize:17,paddingTop:20}} onPress={()=>this.dismiss()}>取消</Text>
                        <Text style={{flex:1,color:themeOrange,textAlign:'center',fontSize:17,paddingTop:20}} onPress={()=>this.commit()}>确定</Text>
                  </View>
              </View>
          </Animated.View>
        )
      }else{
        return null
      }
    }
}

var styles = StyleSheet.create({
    container:{
        flex:1,
        position:'absolute',
        left:0,
        top:0,
        right:0,
        bottom:0,
        backgroundColor:'rgba(0,0,0,0.5)',
        justifyContent:'center',
        alignItems:'center',
    },
    content:{
      height:150,
      width:ScreenWidth-30,
      backgroundColor:'#FFF'
    }
})
