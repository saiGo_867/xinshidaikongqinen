import React,{Component} from 'React'
import { View,StyleSheet } from 'react-native'
import commonVariable from '../commonVariable'
export default class MyProgressVeiw extends Component{

  constructor(props){
      super(props)
  }

  render(){

    return(
       <View style={styles.progress}>
            <View style={[styles.valueProgress,{width:`${this.props.value}%`}]}/>
       </View>
    )
  }
}
MyProgressVeiw.propTypes = {
  value:React.PropTypes.number.isRequired
}

var styles = StyleSheet.create({
    progress:{
        marginTop:commonVariable.scaleHorizon(10),
        marginLeft:commonVariable.scaleHorizon(50),
        marginRight:commonVariable.scaleHorizon(50),
        height:commonVariable.scaleVectical(20),
        backgroundColor:inputBorderColor,
        borderRadius:commonVariable.scaleVectical(10)
    },
    valueProgress:{
      flex:1,backgroundColor:themeOrange,
      borderRadius:commonVariable.scaleVectical(10),
    }
})
