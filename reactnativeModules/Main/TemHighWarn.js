import { View,Image,StyleSheet,Text,TextInput,Button,Animated,Easing } from 'react-native'
import React, { Component } from 'react';

export default class TemHighWarn extends Component {
    constructor(props){
        super(props)
        this.state = {
          isShow: false,
          fadeOutOpacity: new Animated.Value(0),
        }
    }

    show(){
      this.setState({ isShow:true },()=>{
          Animated.timing(this.state.fadeOutOpacity, {
              toValue: 1,
              duration: 500,
              easing: Easing.linear,// 线性的渐变函数
          }).start()
      })
    }

    dismiss(){
      Animated.timing(this.state.fadeOutOpacity, {
          toValue: 0,
          duration: 2000,
          easing: Easing.linear,// 线性的渐变函数
      }).start(()=>{
          this.setState({isShow:false})
      });
    }

    setUI(){
      if (this.state.isShow) {
        return(
          <Animated.View style={[styles.background,{opacity:this.state.fadeOutOpacity}]}>
                <Image style={styles.image} source={require('.././img/8热水器主界面之警告提示/警告.png')}/>
                <Text style={{marginTop:18,marginLeft:10,fontSize:17,fontWeight:'bold',color:textColor}}>警告!已超出恒温。</Text>
                <Text style={{position:'absolute',top:45,left:75,fontSize:14,fontWeight:'bold',color:'#999'}}>温度过高容易烫伤哦!</Text>
          </Animated.View>
        )
      }else{
        return null
      }
    }

    render(){
        return this.setUI()
    }
}

const styles = StyleSheet.create({
    container:{
      alignSelf: 'stretch', //非常重要，覆盖父样式
      alignItems: 'center',
      justifyContent: 'center',
      position:'absolute',  //声明绝对定位
      top:0,
      width:ScreenWidth,
      height:ScreenHeight-88,
      flex: 1,
    },
    background:{
      justifyContent:'center',
      top:220,
      position:'absolute',  //声明绝对定位
      flexDirection:'row',
      width:ScreenWidth-100,
      height:80,
      backgroundColor:'#000'
    },
    image:{
      marginTop:20
    }
})
