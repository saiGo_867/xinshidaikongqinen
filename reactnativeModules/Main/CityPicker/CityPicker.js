import React, {Component,PropTypes} from 'react';
import {PickerIOS,Text,View,Modal,TouchableOpacity,StyleSheet} from 'react-native';

var PickerItemIOS = PickerIOS.Item
export default class CityPicker extends Component {

    constructor(){
      super()
      this.state = {
          modalVisible: false,
          ProvinceIndex: 0,
          provinceSelected:cityData[0]['name'],
          CityIndex:0,
          citySelected:cityData[0]['city'][0]['name'],
          countrySelected:cityData[0]['city'][0]['area'][0]
      }
    }

    provinceChoose(option){
      console.log(option);
      var self = this
      cityData.map((item,i)=>{
          if (item['name'] === option) {
             this.setState({
                ProvinceIndex:i,
                provinceSelected:option,
                CityIndex:0,
                citySelected:cityData[i]['city'][0]['name'],
                countrySelected:cityData[i]['city'][0]['area'][0]
             }, this.callBack)
          }
      })
    }

    cityChoose(option){
        var ProvinceIndex = this.state.ProvinceIndex
        cityData[this.state.ProvinceIndex]['city'].map((item,i)=>{
          if(item['name'] === option){
              this.setState({
                  CityIndex:i,
                  citySelected:option,
                  countrySelected:cityData[ProvinceIndex]['city'][i]['area'][0]
              }, this.callBack);
          }
        })
    }

    countryChoose(option){
        this.setState({countrySelected: option}, this.callBack)
        // var CityIndex = this.state.CityIndex
        // cityData[this.state.CityIndex]
    }

    callBack(){
      if(this.props.onSubmit) {
          this.props.onSubmit(this.state.provinceSelected,this.state.citySelected,this.state.countrySelected)
      }
    }

    render(){
      var province = this.state.provinceSelected;
      var CityIndex = this.state.CityIndex;
      var city = cityData[this.state.ProvinceIndex]['city']||cityData[0]['city'];
      var country = cityData[this.state.ProvinceIndex]['city'][CityIndex]['area']||cityData[0]['city'][0]['area'];
      return(
        <Modal animationType={'slide'} transparent={true} visible={this.state.modalVisible}>
             <View style={styles.basicContainer}>
                  <TouchableOpacity onPress={()=>this.setState({modalVisible: false}) }
                                    style={{ position:'absolute',top:0,right:0,left:0,
                                             bottom:0,backgroundColor:'transparent',opacity:0.5 }}>
                  </TouchableOpacity>
                  <View style={styles.modalContainer}>
                       <View style={styles.mainBox}>
                           <PickerIOS
                               ref={'picker'}
                               style={styles.bottomPicker}
                               selectedValue={this.state.provinceSelected}
                               itemStyle={styles.pickerItem}
                               onValueChange={ (value)=> this.provinceChoose(value) }
                               >
                               {cityData.map((option, i) => {
                                   var label = option['name'];
                                   return (
                                       <PickerItemIOS
                                           value={option['name']}
                                           label={label}
                                           key={i}
                                        />
                                   )
                               })}
                           </PickerIOS>
                           <PickerIOS
                               ref={'picker'}
                               style={styles.bottomPicker}
                               selectedValue={this.state.citySelected}
                               itemStyle={styles.pickerItem}
                               onValueChange={ (value)=> this.cityChoose(value)}
                               >
                               {city.map((option, i) => {
                                   var label = option['name'];
                                   return (
                                       <PickerItemIOS
                                           value={option['name']}
                                           key={i}
                                           label={label}
                                       />
                                   )
                               })}
                           </PickerIOS>
                           <PickerIOS
                               ref={'picker'}
                               style={styles.bottomPicker}
                               selectedValue={this.state.countrySelected}
                               itemStyle={styles.pickerItem}
                               onValueChange={(option) => this.countryChoose(option) }
                               >
                               {country.map((option, i) => {
                                   var label = option;
                                   return (
                                       <PickerItemIOS
                                           value={option}
                                           key={i}
                                           label={label}
                                       />
                                   )
                               })}
                           </PickerIOS>
                       </View>
                     <TouchableOpacity onPress={()=>this.setState({modalVisible: false}) } style={styles.buttonView}>
                         <Text style={{color:themeOrange,fontSize:18}}>确定</Text>
                     </TouchableOpacity>
                  </View>
            </View>
           </Modal>
      )
    }
}

var styles = StyleSheet.create({
    basicContainer:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalContainer:{
        position:'absolute',
        right:0,
        left:0,
        bottom:0,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    buttonView:{
        position:'absolute',
        width:ScreenWidth,
        flex:1,
        bottom:0,
        height:40,
        borderTopWidth:0.5,
        borderTopColor:inputBorderColor,
        backgroundColor:'white',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection:'row',
    },
    bottomPicker : {
        flex:1,
        backgroundColor:'white',
    },
    pickerItem:{
        fontSize: 18,
        color: textPlaceholderColor,
        textAlign: 'center',
        fontWeight: 'bold'
    },
    mainBox: {
        flexDirection:'row',
        marginBottom:40,
    }
})
