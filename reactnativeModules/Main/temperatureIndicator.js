import React, { Component } from 'react';
import { ART,View,Image,StyleSheet,AsyncStorage,TouchableOpacity,TouchableHighlight,Text,TextInput,Button,Animated } from 'react-native'
import commonVariable from '../commonVariable'

const {Surface, Shape, Path, Group, Transform} = ART
var Morph = require('art/morph/path')
let radius = 500
var wavePaths = []
export default class temperatureIndicator extends Component{

    constructor(props){
        super(props)
        for(var j=0; j <= commonVariable.scaleHorizon(radius); j+=10){
            let path =  Morph.Path().moveTo(commonVariable.scaleHorizon(0),commonVariable.scaleHorizon(308))
            for (var i = 0; i <= commonVariable.scaleHorizon(radius); i++) {
                let y = commonVariable.scaleVectical(20) * Math.sin(i * 2 * Math.PI / commonVariable.scaleVectical(radius)+j*2*Math.PI/commonVariable.scaleHorizon(radius))+commonVariable.scaleVectical(308)
                path.lineTo(i,y)
            }
            path.lineTo(commonVariable.scaleHorizon(radius),commonVariable.scaleHorizon(radius))
                .lineTo(0,commonVariable.scaleHorizon(radius))
                .close()
            // arc(x, y, rx, ry, outer):
            // path.arc(0,commonVariable.scaleHorizon(radius)*2,commonVariable.scaleHorizon(radius))
            wavePaths.push(path)
        }
        this.state = {

            // currentTemperature:100,
            transition: Morph.Tween(wavePaths[0], wavePaths[1])
        }

        //页面是否被销毁的标志
        this.lock = false
    }

    componentWillMount(){
      this.current = 1;
    }

    componentDidMount(){
      this.animate(null)
    }

    componentWillUnmount(){
        this.lock = true
    }

    animate(start) {
        //看看页面是否已经被销毁
        if (this.lock) {
          return
        }
        requestAnimationFrame((timestamp) => {
            if (!start){ start = timestamp }
            var delta = (timestamp - start) / 10;
            if (delta > 1) {
              //看看页面是否已经被销毁
              if (this.lock) {
                return
              }
              return this.nextAnimation()
            }
            this.state.transition.tween(delta);
            this.animate(start);
        })
    }

    nextAnimation() {
        this.current += 1
        if (this.current >= wavePaths.length) {
            this.current=1
            this.setState({transition: Morph.Tween(wavePaths[0], wavePaths[1])}, () => this.animate(null, this.nextAnimation))
            return
        }
        this.setState({transition: Morph.Tween(wavePaths[this.current - 1], wavePaths[this.current])}, ()=>this.animate(null, this.nextAnimation))
    }

    draw(){
        // return (<View key={`${i}`} style={{backgroundColor:textPlaceholderColor,width:commonVariable.scaleHorizon(6),
        //                                    height:commonVariable.scaleVectical(40),position:'absolute',
        //                                    transform:[{translateX:Math.sin(i*Math.PI/180)*commonVariable.scaleHorizon(334)},
        //                                               {translateY:-Math.cos(i*Math.PI/180)*commonVariable.scaleHorizon(334)},{rotateZ:(`${i}deg`)}]}}>
        //
        //        </View>)
        let view = []
        for (var i = 210; i <= 510; i+=5) {
            let path = new Path().moveTo(commonVariable.scaleHorizon(374),commonVariable.scaleHorizon(374))
                                 .lineTo(commonVariable.scaleHorizon(374),commonVariable.scaleHorizon(414))
            let x = Math.sin(i*Math.PI/180)*commonVariable.scaleHorizon(334)
            let y = -Math.cos(i*Math.PI/180)*commonVariable.scaleHorizon(334)

            let rotateZ = i
            let strokeColor = inputBorderColor
            if (this.props.hardWareOpen) {
              if (i<=this.props.currentTemperature*5+210) {
                  strokeColor = `hsl(${240-(i-210)/5*4}, 80%, 50%)`
              }
            }
            view.push(
            <Shape key={`${i}`} d={path} stroke={strokeColor} strokeWidth={commonVariable.scaleHorizon(6)}
                   transform={new Transform().translate(x,y).rotate(i,commonVariable.scaleHorizon(374),commonVariable.scaleHorizon(374))}/>
            )
        }
        return(
            <Surface width={commonVariable.scaleHorizon(748)} height={commonVariable.scaleHorizon(748) }>
                  <Group>
                      {view}
                  </Group>
            </Surface>
        )
    }

    turnOnUI(){
      return (
        <View style={styles.innerCircle}>
              <Text style={{textAlign: 'center',color:textColor,fontSize:commonVariable.scaleHorizon(180)}}>{this.props.currentTemperature}°</Text>
              <Text key={'currentTemperatureText'} style={{textAlign: 'center',fontWeight: 'bold',color:textColor,fontSize:commonVariable.scaleHorizon(40)}}>当前温度</Text>
        </View>
      )
    }

    turnOffUI(){
      // <View style={[styles.innerCircle,{shadowColor:'#FFF',shadowOpacity:0.1, }]}>
      // </View>
      return (
          <View style={styles.innerCircle} >
                <Surface width={commonVariable.scaleHorizon(radius)} height={commonVariable.scaleHorizon(radius)}>
                        <Shape d={this.state.transition} stroke='#0077F9FF' fill='#0077F9FF' strokeWidth={1} />
                </Surface>
                <Text style={{color:textColor,fontSize:commonVariable.scaleHorizon(60),position:'absolute',textAlign:'center',
                              fontWeight: 'bold',bottom:commonVariable.scaleVectical(227),width:commonVariable.scaleHorizon(radius)}}>
                    睡眠中...
                </Text>
          </View>
      )
    }


    render(){

        // let view = [];
        // for(i = 0;i <= 360;i+=3.6){
        //     view.push(this.draw(i))
        // }
        return(
            <View style={styles.base}>
                { this.draw() }
                { this.props.hardWareOpen ? this.turnOnUI() : this.turnOffUI() }
            </View>
        )
    }
}

temperatureIndicator.propTypes = {
  hardWareOpen : React.PropTypes.bool.isRequired,
  currentTemperature: React.PropTypes.number.isRequired,
}

var styles = StyleSheet.create({
    base: {
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:themeBackgroundColor,
        width:commonVariable.scaleHorizon(748),
        height:commonVariable.scaleHorizon(748),
    },
    innerCircle:{
      overflow:'hidden',
      alignItems:'center',
      justifyContent:'center',
      position:'absolute',
      backgroundColor:themeBackgroundColor,
      width:commonVariable.scaleVectical(500),
      height:commonVariable.scaleVectical(500),
      borderColor:textPlaceholderColor,
      borderRadius:commonVariable.scaleVectical(500)*0.5,
      borderWidth:0.5,
    }
})
