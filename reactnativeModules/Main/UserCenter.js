import React, { Component } from 'react';
import { ART,View,Image,StyleSheet,AsyncStorage,TouchableOpacity,TouchableHighlight,Text,TextInput,Button,ListView } from 'react-native'
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation'
import commonVariable from '../commonVariable'
import MyHud from '../Utils/MyHud'
import ServerData from '../ServerData'
const {Surface, Shape, Path} = ART
class UserCenter extends Component {

    static navigationOptions = ({navigation})=> ({
        header:null,
        gesturesEnabled:false
    })

    constructor(props){
        super(props)
        //this.state = {a:0}

    }

    //componentDidMount(){
    //    this.interval = setInterval(()=>{
    //        this.setState({
    //            a:this.state.a + 0.2
    //        })
    //    },100)
    //}

    exit(){
      const resetAction = NavigationActions.reset({
                              index: 0,
                              actions: [
                                NavigationActions.navigate({ routeName: 'Login'})
                              ]
      })
      this.props.navigation.dispatch(resetAction)
    }

    renderListItem(rowData){
        return(
          <View style={styles.row}>
                <Image style={{marginLeft:commonVariable.scaleHorizon(50)}} source={rowData[0]}/>
                <Text style={styles.rowDes}>{rowData[1]}</Text>
                <Text style={styles.rowCotent}>{rowData[2]}</Text>
          </View>
        )
    }

    renderSeparator(){
        return(
          <View style={{height:0.5,backgroundColor:inputBorderColor}}>
          </View>
        )
    }

    returnScroll(){
      return(<View/>)
    }

    render(){
        //console.log(this.state.a)
        let serverData = new ServerData()
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
        let data = [[require('../img/9个人中心/性别.png'),'性别:  ',serverData.userMessage.u_sex],[require('../img/9个人中心/电话.png'),'电话: ',serverData.userMessage.u_mobile],
                    [require('../img/9个人中心/邮箱.png'),'邮箱:  ',serverData.userMessage.u_mail],[require('../img/9个人中心/地址.png'),'地址: ',serverData.userMessage.u_address]]

        this.state = { dataSource: ds.cloneWithRows(data),
                       reloadView:false
                     }


        const path = new Path()
            .lineTo(0,commonVariable.scaleVectical(350))
        var i=0
        while(i<ScreenWidth){

            let y = -commonVariable.scaleVectical(40) * Math.sin(i * 5 * Math.PI / ScreenWidth)+commonVariable.scaleVectical(354)
            path.lineTo(i,y)
            i++
        }
        path.lineTo(ScreenWidth,0).close()
        return(
            <View style={styles.container}>
                <Surface width={ScreenWidth} height={commonVariable.scaleVectical(394)}>
                        <Shape d={path} stroke="#000000" fill={themeOrange} strokeWidth={1} />
                </Surface>
                <Image style={{position:'absolute',top:commonVariable.scaleVectical(106),left:commonVariable.scaleHorizon(236)}}
                       source={require('../img/9个人中心/头像.png')}/>
                <Text style={{position:'absolute',top:commonVariable.scaleVectical(136),color:textColor,backgroundColor:themeOrange,
                              fontWeight: 'bold',left:commonVariable.scaleHorizon(458),fontSize:commonVariable.scaleHorizon(55)}}>{serverData.userMessage.u_realname}</Text>
                <Text style={{position:'absolute',marginTop:commonVariable.scaleVectical(196),color:textColor,backgroundColor:themeOrange,
                              fontWeight: 'bold',left:commonVariable.scaleHorizon(458),fontSize:commonVariable.scaleHorizon(55)}}>{serverData.userMessage.u_mobile}</Text>
                <TouchableHighlight style={[styles.row,{marginTop:commonVariable.scaleVectical(125)}]} onPress={()=>this.exit()}>
                      <View style={[styles.row,{flex:1,borderBottomWidth:0.5,borderColor:inputBorderColor}]}>
                            <Image style={{marginLeft:commonVariable.scaleHorizon(50)}} source={require('../img/9个人中心/退出帐号.png')}/>
                            <Text style={styles.rowDes}>退出帐号</Text>
                      </View>
                </TouchableHighlight>
                <ListView dataSource={this.state.dataSource} renderRow={(rowData)=>this.renderListItem(rowData)}
                          renderSeparator={()=>this.renderSeparator()} renderScrollComponent= {()=>this.returnScroll()}>
                </ListView>
                <View style={{flex:1,alignItems:'center'}}>
                      <TouchableHighlight style={{marginTop:commonVariable.scaleVectical(110),width:commonVariable.scaleHorizon(600),alignItems:'center',justifyContent:'center',
                                                  height:commonVariable.scaleVectical(130),borderRadius:commonVariable.scaleVectical(65),borderColor:inputBorderColor,borderWidth:0.5}}
                                          onPress={ ()=>this.props.navigation.navigate('UserMessageEdit', {
                                                       // 跳转的时候携带一个参数去下个页面
                                                       callback: ()=>{
                                                             this.setState({reloadView:true})
                                                       }
                                               }) }>
                            <Text style={{color:textPlaceholderColor,fontSize:commonVariable.scaleHorizon(45),fontWeight:'bold'}}>编辑资料>></Text>
                      </TouchableHighlight>
                </View>
            </View>
        )
    }
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: themeBackgroundColor
    },
    row: {
        flexDirection:'row',
        alignItems:'center',
        width:ScreenWidth,
        height:commonVariable.scaleVectical(150),
    },
    rowDes:{
        color:textPlaceholderColor,
        fontWeight: 'bold',
        fontSize:commonVariable.scaleHorizon(50),
        marginLeft:commonVariable.scaleHorizon(30)
    },
    rowCotent:{
        color:textColor,
        fontSize:commonVariable.scaleHorizon(50)
    }
})

export default connect(state => ({
    routes: state.nav.routes
}), dispatch => ({}))(UserCenter);
