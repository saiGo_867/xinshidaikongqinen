import React, { Component } from 'react';
import { View,Image,StyleSheet,Animated,AsyncStorage,DeviceEventEmitter,TouchableHighlight,Text,Easing,TextInput,Button,NativeModules } from 'react-native'
import { connect } from 'react-redux'
import InputSNView from './InputSNView'
import commonVariable from '../commonVariable'
import MyHud from '../Utils/MyHud'
import TemperatureIndicator from './temperatureIndicator'
import MyProgressView from './MyProgressView'
class Control extends Component {

    static navigationOptions = ({navigation})=> ({
        headerLeft:<TouchableHighlight>
                        <Image style={{left:commonVariable.scaleHorizon(40)}} source={require('../img/6热水器主界面/菜单.png')}/>
                   </TouchableHighlight>,
        headerRight:<TouchableHighlight onPress={()=>DeviceEventEmitter.emit('openSNInput')}>
                        <Image style={{right:commonVariable.scaleHorizon(40)}} source={require('../img/6热水器主界面/添加设备.png')}/>
                    </TouchableHighlight>,
        gesturesEnabled:false
    })

    constructor(props){
      super(props)
      this.state={
        hardWareOpen:true,
        thisUseFlw:0,
        allUseFlw:0,
        openTemperature:false,
        currentTemperature:35,
        setTemperature:35,
        onLine:true,
        rotateValue: new Animated.Value(0)
      }
      this.disableControl = false

      this.openSNInputSubscribe = DeviceEventEmitter.addListener('openSNInput',()=>{this.showInputSN()})

      //处理消息
      var {NativeAppEventEmitter} = require('react-native')
      //监听MQTT返回消息
      NativeModules.RNHelper.mqttObserveMessage(this.props.temporarySN)

      //向事件接收器注册名为NativeModuleMsg的事件,并且指定收到事件后的处理函数
      this.NativeMsgSubscription = NativeAppEventEmitter.addListener(
        'MQTTMessage',(reminder)=>{
              this.handleMessage(reminder.MQTTMessage)
         }
      )
    }

    componentWillUnmount(){
        this.openSNInputSubscribe.remove()
        this.NativeMsgSubscription.remove();
    }

    showInputSN(){
        this.refs.inputSN.show()
    }

    //处理MQTT消息
    handleMessage(message){
        console.log(`${message}消息`);
        let m = message
        m = m.replace(this.props.temporarySN,'')
        if (m.indexOf('retoc') !== -1) {
            m = m.replace('retoc','')
            if (m.indexOf('1') !== -1) { //检查是否是开
                this.setState({
                     hardWareOpen : true
                })
            }else{
                this.setState({
                     hardWareOpen : false
                })
            }
        }

        if (m.indexOf('retst') !== -1) {
            m = m.replace('retst','')
            this.setState({
                setTemperature: parseInt(m)
            })
        }

        if (m.indexOf('retlx') !== -1) {
            m = m.replace('retlx','')
            if (message.indexOf('1') !== -1) { //检查是否是强制断开
                // 强制断开
            }else{
                //恢复正常
            }
        }

        if (m.indexOf('dathe') !== -1) {
            m = m.replace('dathe','')
            let arr = m.split('|')
            this.setState({
                  currentTemperature:parseInt(arr[0]),
                  setTemperature:parseInt(arr[2]),
                  hardWareOpen:arr[3] === '1',
                  onLine:true
            })
        }

        if (m.indexOf('datfl') !== -1) {
            m = m.replace('datfl','')
            let arr = m.split('|')
            this.setState({
                thisUseFlw:arr[0],
                allUseFlw:arr[1],
            })
        }

        if (m.indexOf('datof') !== -1) {
            //设备离线
            this.setState({
              onLine:false
            })
        }

        if (m.indexOf('datip') !== -1) {
            //设备ip地址
        }
    }

    changeUI(){
      this.setState({
        openTemperature:!this.state.openTemperature
      })
    }

    addTem(){
        if (this.state.setTemperature >= 55){
            this.refs.hud.showWithInfo('已达到可设定最高温度!')
            return
        }
        if (this.disableControl) {
            return
        }
        // this.state.setTemperature === 100 ? null : this.setState({setTemperature:this.state.setTemperature+1})
        //原生加温
        NativeModules.RNHelper.addTemperatureAirHeater(this.props.temporarySN)
        this.disableControl = true
        setTimeout( () => { this.disableControl = false },3000)
    }

    decreaseTem(){
        if (this.state.setTemperature <= 20){
            this.refs.hud.showWithInfo('已达到可设定最低温度!')
            return
        }
        if (this.disableControl) {
            return
        }
        // this.state.setTemperature === 0 ? null : this.setState({setTemperature:this.state.setTemperature-1})
        //原生降温
        NativeModules.RNHelper.subTemperatureAirHeater(this.props.temporarySN)
        this.disableControl = true
        setTimeout( () => { this.disableControl = false },3000)
    }

    controlHardware(){
        if (this.disableControl) {
            return
        }
        // this.refs.temperature.setState({
        //     hardWareOpen : !this.refs.temperature.state.hardWareOpen
        // })

        //判断是开机还是关机
        if (this.state.hardWareOpen === false) {
            //原生开机
            NativeModules.RNHelper.openAirWaterHeater(this.props.temporarySN)
        }else{
            //原生关机
            NativeModules.RNHelper.closeAirWaterHeater(this.props.temporarySN)
        }

        this.disableControl = true
        setTimeout( () => { this.disableControl = false },3000)
    }

    middleView(){
      let arrow = this.state.openTemperature === true ? require('../img/7热水器主界面之温度设置点击/向下键.png') : require('../img/5热水器主界面之关机状态/前进键.png')
      let view = []
      if (this.state.openTemperature) {
          view.push(
                <TouchableHighlight key='c' style={{flex:1,flexDirection:'row',alignItems:'center',marginTop:commonVariable.scaleVectical(60),width:ScreenWidth}}
                                    underlayColor={themeBackgroundColor} onPress={()=>this.changeUI()}>
                    <View style={{flex:1,flexDirection:'row',alignItems:'center'}} >
                          <Image style={styles.image} source={require('../img/5热水器主界面之关机状态/温度设置图标.png')}/>
                          <View style={styles.bottomText}>
                                <Text style={{color:textColor,fontSize:commonVariable.scaleHorizon(50),fontWeight:'bold',marginBottom:commonVariable.scaleVectical(23)}}>温度设置</Text>
                                <Text style={{color:textPlaceholderColor,fontSize:commonVariable.scaleHorizon(40),fontWeight:'bold'}}>注意手速,不要过快哦!</Text>
                          </View>
                          <Image style={{right:commonVariable.scaleHorizon(40),position:'absolute'}} source={arrow} />
                    </View>
                </TouchableHighlight>
          )
          view.push(
                <View key='d' style={{flex:1,width:ScreenWidth}}>
                      <Text style={{color:textColor,fontWeight: 'bold',fontSize:commonVariable.scaleHorizon(55),textAlign:'right',
                                    marginRight:(ScreenWidth-commonVariable.scaleHorizon(100))*(100-this.state.setTemperature)/100}}
                            ref='temperatureText'>{this.state.setTemperature}°</Text>
                      <MyProgressView style={styles.progress} value={this.state.setTemperature}/>
                      <View style={{flexDirection:'row',flex:1,marginTop:commonVariable.scaleVectical(40),
                                    marginLeft:commonVariable.scaleHorizon(50),marginRight:commonVariable.scaleHorizon(50)}}>
                            <TouchableHighlight style={{flex:1}} underlayColor= {themeBackgroundColor}
                                                onPress={ () => this.decreaseTem() }>
                                  <Image source={require('../img/7热水器主界面之温度设置点击/减.png')}/>
                            </TouchableHighlight>
                            <TouchableHighlight style={{flex:1,alignItems:'flex-end'}} underlayColor={themeBackgroundColor}
                                                onPress={ () => this.addTem() }>
                                  <Image source={require('../img/7热水器主界面之温度设置点击/加.png')}/>
                            </TouchableHighlight>
                      </View>
                </View>
          )
      }else{
          view.push(
                <View key='a' style={{flexDirection:'row',marginTop:commonVariable.scaleVectical(40),width:ScreenWidth,
                        height:commonVariable.scaleVectical(200)}}>
                      <View style={[styles.board,{marginLeft:commonVariable.scaleHorizon(30),marginRight:commonVariable.scaleHorizon(25)}]}>
                            <Text style={styles.dataText}>{this.state.thisUseFlw}L</Text>
                            <Text style={styles.desDataText}>本次用量</Text>
                      </View>
                      <View style={[styles.board,{marginLeft:commonVariable.scaleHorizon(25),marginRight:commonVariable.scaleHorizon(30)}]}>
                            <Text style={styles.dataText}>{this.state.allUseFlw}L</Text>
                            <Text style={styles.desDataText}>累计用量</Text>
                      </View>
                </View>
              )
          view.push(
                <TouchableHighlight key='b' style={{flex:1,flexDirection:'row',alignItems:'center',marginTop:commonVariable.scaleVectical(110),width:ScreenWidth}} onPress={()=>this.changeUI()}>
                    <View style={{flex:1,flexDirection:'row',alignItems:'center'}} >
                          <Image style={styles.image} source={require('../img/5热水器主界面之关机状态/温度设置图标.png')}/>
                          <View style={styles.bottomText}>
                                <Text style={{color:textColor,fontSize:commonVariable.scaleHorizon(50),fontWeight:'bold',marginBottom:commonVariable.scaleVectical(23)}}>温度设置</Text>
                                <Text style={{color:textPlaceholderColor,fontSize:commonVariable.scaleHorizon(40),fontWeight:'bold'}}>注意手速,不要过快哦!</Text>
                          </View>
                          <Image style={{right:commonVariable.scaleHorizon(40),position:'absolute'}} source={arrow} />
                    </View>
                </TouchableHighlight>
          )
      }
      return view
    }

    onLineView(){
      switchSource = this.state.hardWareOpen ? require('../img/5热水器主界面之关机状态/开.png') : require('../img/5热水器主界面之关机状态/关.png')
      return(
          <View style={styles.container}>
              <TemperatureIndicator ref='temperature' currentTemperature={this.state.currentTemperature} hardWareOpen={this.state.hardWareOpen}/>
              { this.middleView() }
              <View style={{flex:1,flexDirection:'row',alignItems:'center',width:ScreenWidth,borderTopWidth:0.5,borderColor:textPlaceholderColor}}>
                    <Image style={styles.image} source={require('../img/5热水器主界面之关机状态/设备状态图标.png')}/>
                    <View style={styles.bottomText}>
                          <Text style={{color:textColor,fontSize:commonVariable.scaleHorizon(50),fontWeight:'bold',marginBottom:commonVariable.scaleVectical(23)}}>设备状态</Text>
                          <Text style={{color:textPlaceholderColor,fontSize:commonVariable.scaleHorizon(40),fontWeight:'bold'}}>开启才会有数据哦!</Text>
                    </View>
                    <TouchableHighlight style={{right:commonVariable.scaleHorizon(40),position:'absolute'}} onPress={()=>this.controlHardware()}>
                          <Image source={switchSource} />
                    </TouchableHighlight>
              </View>
              <MyHud ref='hud'/>
              <InputSNView ref='inputSN'/>
          </View>
      )
    }

    offLineView(){
        return(
            <View style={styles.container}>
                <Image style={{marginTop:commonVariable.scaleVectical(110),marginBottom:commonVariable.scaleVectical(70)}} source={require('../img/6热水器主界面/连接不上的图.png')}/>
                <Text style={{fontSize:commonVariable.scaleHorizon(55),marginBottom:commonVariable.scaleVectical(45),fontWeight:'bold',color:textColor,textAlign:'center'}}>是的,设备失踪了</Text>
                <Text style={{fontSize:commonVariable.scaleHorizon(45),fontWeight:'bold',color:textPlaceholderColor,textAlign:'center'}}>原因什么的我真不知道哦 T.T</Text>
                <Text style={{fontSize:commonVariable.scaleHorizon(45),marginBottom:commonVariable.scaleVectical(80),fontWeight:'bold',color:textPlaceholderColor,textAlign:'center'}}>检查一下设备吧</Text>
                <TouchableHighlight style={{flexDirection:'row',width:commonVariable.scaleHorizon(360),height:commonVariable.scaleVectical(140),
                                            borderColor:inputBorderColor,borderWidth:1,borderRadius:5}}
                                    onPress={()=>{
                                              this.state.rotateValue.setValue(0);
                                              Animated.timing( // 从时间范围映射到渐变的值。
                                                               this.state.rotateValue, {
                                                                 toValue: 3,
                                                                 duration: 3000,// 动画持续的时间（单位是毫秒），默认为500
                                                                 easing: Easing.out(Easing.quad),// 一个用于定义曲线的渐变函数
                                                                 delay: 0,// 在一段时间之后开始动画（单位是毫秒），默认为0。
                                                              }).start()
                                    }}>
                    <View style={{flexDirection:'row',flex:1,justifyContent:'center',alignItems:'center'}}>
                        <Text style={{fontSize:commonVariable.scaleHorizon(55),color:textColor,fontWeight:'bold',textAlign:'center'}}>刷新页面 </Text>
                        <Animated.Image style={{transform:[{rotate: this.state.rotateValue.interpolate({
                                                                                      // 旋转，使用插值函数做值映射
  		                                                                                inputRange: [0, 1],
  		                                                                                outputRange: ['0deg', '360deg'],
  		                                                 })}]
                                              }} source={require('../img/6热水器主界面/形状-138.png')}/>
                    </View>
                </TouchableHighlight>
                <InputSNView ref='inputSN'/>
            </View>
        )
    }

    render(){
      if (this.state.onLine) {
          return this.onLineView()
      }else{
         return this.offLineView()
      }
    }
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: themeBackgroundColor
    },
    board:{
      flex:1,
      alignItems:'center',
      justifyContent:'center',
      borderRadius:5,
      backgroundColor:'#1F2635',
      height:commonVariable.scaleVectical(200)
    },
    dataText:{
      flex:1,
      paddingTop:commonVariable.scaleVectical(30),
      textAlign:'center',
      color:textColor,
      fontSize:commonVariable.scaleHorizon(65),
    },
    desDataText:{
      flex:1,
      textAlign:'center',
      color:textPlaceholderColor,
      fontSize:commonVariable.scaleHorizon(50)
    },
    image:{
      marginLeft:commonVariable.scaleHorizon(55)
    },
    bottomText:{
      flex:1,
      marginRight:commonVariable.scaleHorizon(-40),
      marginLeft:commonVariable.scaleHorizon(35)
    },
})

export default connect(state => ({
    routes: state.nav.routes,
    temporarySN:state.controlOrder.temporarySN
}), dispatch => ({}))(Control);
