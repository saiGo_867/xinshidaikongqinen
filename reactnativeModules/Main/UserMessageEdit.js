import React, { Component } from 'react';
import { View,Image,StyleSheet,AsyncStorage,TouchableOpacity,TouchableHighlight,Text,TextInput,Button,ListView,Alert } from 'react-native'
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation'
import commonVariable from '../commonVariable'
import ServerData from '../ServerData'
import LoadingHud from '../Utils/LoadingHud'
import NetUtil from '../Utils/NetUtil'
import MyHud from '../Utils/MyHud'
import CityPicker from './CityPicker/CityPicker'
class UserMessageEdit extends Component {

  static navigationOptions = ({navigation})=> ({
      headerTitle:'资料修改',
      headerLeft:<TouchableHighlight onPress={()=>navigation.goBack()}>
                      <Image style={{left:commonVariable.scaleHorizon(40)}} source={require('../img/3注册页/返回键.png')}/>
                 </TouchableHighlight>
  })

    constructor(props){
      super(props)
      const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
      let serverData = new ServerData()
      let data = [['姓名: ',serverData.userMessage.u_realname,'请输入你的真实姓名'],
                  ['邮箱:  ',serverData.userMessage.u_mail,'请输入你的邮箱']]
      this.state = {
          dataSource: ds.cloneWithRows(data),
          sex: serverData.userMessage.u_sex,
          address:serverData.userMessage.u_address.split('*')[0],
          detailAddress:serverData.userMessage.u_address.split('*')[1],
      }
      this.topUserData = [serverData.userMessage.u_realname,serverData.userMessage.u_mail]
    }

    renderListItem(rowData,rowID,sectionID){
        return(
          <View style={styles.row}>
                <Text style={styles.rowDes}>{rowData[0]}</Text>
                <TextInput style={styles.rowCotent} defaultValue={rowData[1]} placeholder={rowData[2]}
                           placeholderTextColor={textPlaceholderColor} onChangeText={(text)=> this.topUserData[sectionID]=text}/>
          </View>
        )
    }

    renderSeparator(){
        return(
          <View style={{height:0.5,backgroundColor:inputBorderColor}}>
          </View>
        )
    }

    returnScroll(){
      return(<View/>)
    }

    upDateUserInfor(){
      var serverData = new ServerData()
      if (this.topUserData[0] === undefined || this.topUserData[0].length === 0) {
        // Alert.alert('请输入你的真实姓名','',[{text: '确定'}])
        this.refs.myhud.showWithInfo('请输入你的真实姓名')
        return
      }
      if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(this.topUserData[1]) !== true) {
        // Alert.alert('请输入正确的邮箱地址','',[{text: '确定'}])
        this.refs.myhud.showWithInfo('请输入正确的邮箱地址')
        return
      }
      if (this.state.address === undefined || this.state.address.length === 0) {
        // Alert.alert('请输入你的地址','',[{text: '确定'}])
        this.refs.myhud.showWithInfo('请输入你的地址')
        return
      }
      if (this.state.detailAddress === undefined || this.state.detailAddress.length === 0) {
        this.refs.myhud.showWithInfo('请输入你的详细地址')
        return
      }
      var paras = { 'uid':serverData.userMessage.u_id,'realname':this.topUserData[0],'address':this.state.address + '*' + this.state.detailAddress,
                    'sex':this.state.sex,'mail':this.topUserData[1] }
      this.refs.loadinghud.show()
      NetUtil.get('UpDateUserInfo',paras,//成功回调
      (response) => {
          this.refs.loadinghud.dismiss()
          if (response === 1) {
            serverData.userMessage.u_realname = this.topUserData[0]
            serverData.userMessage.u_mail = this.topUserData[1]
            serverData.userMessage.u_sex = this.state.sex;
            serverData.userMessage.u_address = this.state.address + '*' + this.state.detailAddress;
            this.props.navigation.state.params.callback()
            Alert.alert('修改成功!','',[{text: '确定',onPress:()=> this.props.navigation.goBack() }])
            // this.refs.myhud.clickOn = {()=>this.props.navigation.goBack()}
            // this.refs.myhud.showWithInfo('修改成功')
          }else{
            // Alert.alert('修改失败,请重试!','',[{text: '确定'}])
            this.refs.myhud.showWithInfo('修改失败,请重试!')
          }
      },
      //失败回调
      (error) => {
         console.log(error)
         this.refs.loadinghud.dismiss()
        //  Alert.alert('请检查你的网络!','',[{text: '确定'}])
        this.refs.myhud.showWithInfo('请检查你的网络!')
      })
    }


    render(){
        var sexMaleImageUri = this.state.sex === '男' ? require('../img/10资料修改/性别选中.png') : require('../img/10资料修改/性别未选中.png')
        var sexFemaleImageUri = this.state.sex === '女' ? require('../img/10资料修改/性别选中.png') : require('../img/10资料修改/性别未选中.png')
        return(
          <View style={styles.container}>
              <View style={styles.topElement}>
                    <View style={styles.headerIcons}>
                        <Text style={styles.rowDes}>头像:</Text>
                        <Image style={{position:'absolute',right:commonVariable.scaleHorizon(50)}} source={require('../img/10资料修改/头像.png')}/>
                    </View>
                    <ListView dataSource={this.state.dataSource} renderRow={(rowData,rowID,sectionID)=>this.renderListItem(rowData,rowID,sectionID)}
                              renderSeparator={()=>this.renderSeparator()} renderScrollComponent= {()=>this.returnScroll()}/>
                    <View style={[styles.row,{borderBottomWidth:0.5,borderColor:inputBorderColor}]}>
                        <Text style={styles.rowDes}>性别:</Text>
                        <TouchableHighlight  onPress={()=>this.setState({sex:'男'})}>
                            <Image style={{marginLeft:commonVariable.scaleHorizon(40),marginRight:commonVariable.scaleHorizon(38)}} source={sexMaleImageUri}/>
                        </TouchableHighlight>
                        <Text style={styles.sexText}>男</Text>
                        <TouchableHighlight onPress={()=>this.setState({sex:'女'})}>
                            <Image style={{marginLeft:commonVariable.scaleHorizon(90),marginRight:commonVariable.scaleHorizon(38)}} source={sexFemaleImageUri}/>
                        </TouchableHighlight>
                        <Text style={styles.sexText}>女</Text>
                    </View>
              </View>
              <View style={[styles.topElement,{flex:1.3,alignItems:'center'}]}>
                    <View style={[styles.row,{borderBottomWidth:0.5,borderColor:inputBorderColor}]}>
                          <Text style={styles.rowDes}>地址: </Text>
                          <Text style={styles.rowCotent} onPress = {() => this.cityPicker.setState({modalVisible:true})} >{this.state.address}</Text>
                    </View>
                    <View style={[styles.row,{borderBottomWidth:0.5,borderColor:inputBorderColor}]}>
                          <Text style={styles.rowDes}>详细地址: </Text>
                          <TextInput style={styles.rowCotent} defaultValue={this.state.detailAddress} placeholder='请输入详细地址以便联系'
                                     placeholderTextColor={textPlaceholderColor} onChangeText={(text)=> this.state.detailAddress=text}/>
                    </View>
                    <TouchableHighlight style={{marginTop:commonVariable.scaleVectical(100),backgroundColor:themeOrange,borderRadius:commonVariable.scaleVectical(55),
                                                width:commonVariable.scaleHorizon(600),height:commonVariable.scaleVectical(110),alignItems:'center',justifyContent:'center'}}
                                        onPress={()=> this.upDateUserInfor() } underlayColor='#A04317'>
                          <Text style={{color:textColor,fontSize:commonVariable.scaleHorizon(50)}}>确认修改</Text>
                    </TouchableHighlight>
              </View>
              <CityPicker ref={(ref) => this.cityPicker = ref}
                          onSubmit={(a,b,c)=> this.setState({ address:a+'·'+b+'·'+c }) } />
              <LoadingHud ref='loadinghud'/>
              <MyHud ref='myhud'/>
          </View>
        )
    }
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000'
    },
    topElement: {
        flex:1,
        marginTop:commonVariable.scaleVectical(40),
        backgroundColor:themeBackgroundColor
    },
    headerIcons:{
      flexDirection:'row',
      alignItems:'center',
      borderBottomWidth:0.5,
      borderColor:inputBorderColor,
      height:commonVariable.scaleVectical(250)
    },
    row: {
        flexDirection:'row',
        alignItems:'center',
        width:ScreenWidth,
        height:commonVariable.scaleVectical(150),
    },
    rowDes:{
        color:textPlaceholderColor,
        fontWeight: 'bold',
        fontSize:commonVariable.scaleHorizon(50),
        marginLeft:commonVariable.scaleHorizon(30)
    },
    rowCotent:{
        flex:1,
        color:textColor,
        fontSize:commonVariable.scaleHorizon(50),
        fontWeight: 'bold',
    },
    sexText:{
      color:textColor,
      fontSize:commonVariable.scaleHorizon(50),
      fontWeight: 'bold',
    },
})
export default connect(state => ({
    routes: state.nav.routes
}), dispatch => ({}))(UserMessageEdit);
