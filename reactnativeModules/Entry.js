import React, { Component } from 'react';
import {
  AppRegistry,Image,View
} from 'react-native';
import { connect } from 'react-redux';
import { addNavigationHelpers } from 'react-navigation'
import Root from './navRoot'

const mapStateToProps = state => ({
    nav: state.nav,
});

class Entry extends Component {
    render(){
      const { dispatch, nav } = this.props;
      console.log(`${this.props.nav}000000`);
        return (
          <Root navigation={addNavigationHelpers({
              dispatch: dispatch,
              state: nav
          })}
          />
        );
    }
}

// const Entry = ({ dispatch, nav }) => (
//     <Root navigation={addNavigationHelpers({ dispatch, state: nav })}/>
// );

export default connect(state => ({ nav: state.nav }))(Entry);
// export default connect(mapStateToProps)(Entry)
