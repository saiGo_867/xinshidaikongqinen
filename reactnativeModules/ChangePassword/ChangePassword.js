import React, { Component } from 'react';
import { View,Image,StyleSheet,AsyncStorage,TouchableOpacity,TouchableHighlight,Text,TextInput,Button,ListView,ScrollView,Alert } from 'react-native'
import { connect } from 'react-redux';
import commonVariable from '../commonVariable'
import MyHud from '../Utils/MyHud'
import LoadingHud from '../Utils/LoadingHud'
import NetUtil from '../Utils/NetUtil'
// import SecCodeButton from '../SecCodeButton'
class ChangePassword extends Component{

  static navigationOptions = ({navigation})=> ({
      headerTitle:'修改密码',
      headerLeft:<TouchableHighlight onPress={()=>navigation.goBack()}>
                      <Image style={{left:commonVariable.scaleHorizon(40)}} source={require('../img/3注册页/返回键.png')}/>
                 </TouchableHighlight>
  })

  constructor(props) {
      super(props)
      const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
      let data = [['手机号码:','请输入手机号码'],['新密码:','请输入新密码'],['确认新密码:','请再次输入新密码'],['验证码:','请填写手机验证码']]
      this.state = {
          dataSource: ds.cloneWithRows(data),
          timerCount: 5,
          timerTitle: "获取验证码",
          selfEnable: false
      }
      this.dataArr = ['','','','']
      this.confirmMobilePhone=''
      this.secCode = this.randomNumberInRange(1000,10000)
  }

  componentWillUnmount(){
      this.interval && clearInterval(this.interval)
  }

  getSecCode(){
    // console.log(this.mobilePhone)
    // if(this.selfEnable === false){
    //    return
    // }
    if (/^1[3|4|5|7|8][0-9]{9}$/.test(this.dataArr[0]) === false) {
      Alert.alert('请输入正确的手机号码!','',[{text: '确定'}])
      return
    }

    NetUtil.receivePasswordMessage(this.dataArr[0],this.secCode,
      //成功回调
      (response) => {
        console.log("======"+response)
    })
    this.confirmMobilePhone = this.dataArr[0]
    this.setState({selfEnable: true})
    this.interval = setInterval(()=>{
        const timer = this.state.timerCount - 1
        if(timer===-2){
          this.interval && clearInterval(this.interval)
          this.setState({
            timerCount: 5,
            timerTitle: "获取验证码",
            selfEnable: false
          })
        }else{
          this.setState({
            timerCount:timer,
            timerTitle: '重新获取('+this.state.timerCount+'s)'
          })
        }
    },1000)
  }

  randomNumberInRange(lowerLimit, upperLimit) {
      return Math.floor(Math.random() * (1 + upperLimit - lowerLimit)) + lowerLimit;
  }

  renderListItem(rowData,rowID,sectionID,){
      return(
        <View style={styles.listRow}>
              <Text style={{color:textColor,fontSize:commonVariable.scaleHorizon(50)}}>{rowData[0]}</Text>
              <TextInput style={styles.textInput} placeholder={rowData[1]} placeholderTextColor={textPlaceholderColor}
                         selectionColor={textColor} underlineColorAndroid = 'transparent'
                         onChangeText={(text) => {
                             this.dataArr[sectionID] = text
                          }}/>
              { rowData[0] === '验证码:' ?
               (<TouchableHighlight disabled={this.state.selfEnable} style={styles.secCodeBtn} onPress={()=>this.getSecCode()}>
                    <Text adjustsFontSizeToFit={true} numberOfLines={1} style={{fontSize:commonVariable.scaleHorizon(40),
                          color:themeBackgroundColor, textAlign:'center'}}>{this.state.timerTitle}</Text>
                </TouchableHighlight>) : null}
        </View>
      )
  }

  renderSeparator(){
      return(
        <View style={{height:0.5,backgroundColor:inputBorderColor}}>
        </View>
      )
  }

  returnScroll(){
    return(<View/>)
  }

  commit(){
    if (this.dataArr[1] === undefined || this.dataArr[1].length<6){
      Alert.alert('密码至少为6位!','',[{text: '确定'}])
      return
    }
    if (this.dataArr[1] !== this.dataArr[2]){
      Alert.alert('两次输入的密码不一致!','',[{text: '确定'}])
      return
    }
    if (this.dataArr[0] !== this.confirmMobilePhone){
      Alert.alert('手机号码不一致!','',[{text: '确定'}])
      return
    }
    if (this.dataArr[3] != this.secCode){
      Alert.alert('验证码不正确!','',[{text: '确定'}])
      return
    }
    var paras = {'u_pass':this.dataArr[2], 'u_mobile':this.confirmMobilePhone}
    this.refs.loadinghud.show()
    NetUtil.get('UpdatePassword',paras,
      //成功回调
      (response) => {
        this.refs.loadinghud.dismiss()
        if(response!==1){
          Alert.alert('此账号不存在!','',[{text: '确定'}])
          return
        }
        Alert.alert('修改成功,请保管好你的新密码!','',[{text: '确定',onPress:()=>{
          // this.props.navigation.state.params.callback(this.mobilePhone)
          this.props.navigation.goBack()
          }
        }])
      },
      //失败回调
      (error) => {
         this.refs.loadinghud.dismiss()
         Alert.alert('请检查你的网络!','',[{text: '确定'}])
    })
  }

  render(){
      return(
        <View style={styles.container}>
              <View style={{height:0.5,backgroundColor:inputBorderColor}}>
              </View>
              <ListView dataSource={this.state.dataSource} renderRow={(rowData,rowID,sectionID)=>this.renderListItem(rowData,rowID,sectionID)}
                        renderSeparator={()=>this.renderSeparator()} renderScrollComponent= {()=>this.returnScroll()}>
              </ListView>
              <TouchableHighlight style={styles.button} underlayColor='#A04317' onPress={()=>this.commit()}>
                    <Text style={styles.text}>立即提交</Text>
              </TouchableHighlight>
              <LoadingHud ref='loadinghud'/>
        </View>
      )
  }
}

var styles = StyleSheet.create({
  container:{
      flex:1,
      backgroundColor:themeBackgroundColor,
  },
  listRow:{
    alignItems:'center',
    flexDirection:'row',
    paddingLeft:commonVariable.scaleHorizon(50),
    height:commonVariable.scaleVectical(150)
  },
  textInput:{
      flex:1,
      color:textColor,
      marginLeft:commonVariable.scaleHorizon(34),
      fontSize:commonVariable.scaleHorizon(45),
  },
  secCodeBtn:{
    justifyContent:'center',
    backgroundColor:'#FFF',
    height:commonVariable.scaleHorizon(90),
    width:85,
    paddingLeft:commonVariable.scaleHorizon(10),
    paddingRight:commonVariable.scaleHorizon(10),
    marginRight:commonVariable.scaleHorizon(20),
    borderRadius:commonVariable.scaleHorizon(90)*0.5,
  },
  button:{
    alignItems:'center',
    justifyContent:'center',
    marginTop:commonVariable.scaleVectical(60),
    marginLeft:commonVariable.scaleVectical(60),
    marginRight:commonVariable.scaleVectical(60),
    height:commonVariable.scaleVectical(130),
    borderRadius:commonVariable.scaleVectical(130)*0.5,
    backgroundColor:themeOrange
  },
  text:{
    color:textColor,
    fontSize:commonVariable.scaleHorizon(55)
  },
})
export default connect(state => ({
    routes: state.nav.routes
}), dispatch => ({}))(ChangePassword);
