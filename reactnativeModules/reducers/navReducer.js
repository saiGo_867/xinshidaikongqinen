import Root from '../navRoot';

const recentlyVisitedRoutes = new Set();//防止連點，多次navigate，增加此判斷
const NavReducers = (state, action) => {
    if (action.type === 'Navigation/NAVIGATE') {
        if (recentlyVisitedRoutes.has(action.routeName)) {
            return state;
        }
        recentlyVisitedRoutes.add(action.routeName);
        setTimeout(() => {
            recentlyVisitedRoutes.delete(action.routeName);
        }, 400);
    }
    const newState = Root.router.getStateForAction(action, state);
    console.log(`${newState}111111`)

    return newState || state;
};

export default NavReducers;
