import { combineReducers } from 'redux';
import nav from './navReducer'
import controlOrder from './controlReducer'
const rootReducer = combineReducers({
    controlOrder,
    nav
});
export default rootReducer
