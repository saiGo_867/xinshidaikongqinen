import {SUBSCRIBENEWSN} from '../Actions/ControlActionTypes'

// 原始默认state
const defaultState = {
  temporarySN:'SN000001',
  onLine:true
}
const controlOrder = (state=defaultState,action)=>{
    switch (action.type) {
      
      case SUBSCRIBENEWSN:
        return {...state,temporarySN:action.payload}
      default:
        return state
    }
}

export default controlOrder
// export default function controlOrder(state=defaultState,action){
// }
