import {View,StyleSheet,ActivityIndicator} from 'react-native';
import React, { Component } from 'react';
import commonVariable from '../commonVariable'
export default class LoadingHud extends Component {
  constructor(){
    super()
    this.state = {isShow: false}
  }

  show(){
    this.setState({isShow: true})
  }

  dismiss(){
    this.setState({isShow: false})
  }

  setUI(){
    if (this.state.isShow === true) {
      return (
        <View style={styles.container} >
        <ActivityIndicator
            animating={true}
            color='white'
            size='large'
        />
        </View>
      )
    }else {
      return null
    }
  }

  render(){
       return(
           this.setUI()
       );
   }
}

var styles = StyleSheet.create ({
  container:{
      alignSelf: 'stretch', //非常重要，覆盖父样式
      alignItems: 'center',
      justifyContent: 'center',
      position:'absolute',  //声明绝对定位
      top:0,
      width:ScreenWidth,
      height:ScreenHeight,
      flex: 1,
      backgroundColor: '#888',
      opacity:0.5
  }
})
