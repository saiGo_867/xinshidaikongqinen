const url = 'http://www.yihuan100.com:82/api/'

//短信平台参数
const messageUrl = 'http://web.cr6868.com/asmx/smsservice.aspx?'
const sign = ''
const name = '13925876719'
const pwd = '9C72D3D823E48EB4273D44744532'
const stime = ''
const type = 'gx'
const extno = ''

export default class NetUitl {
    /*
     *  get请求
     *  url:请求地址
     *  data:参数
     *  callback:回调函数
     * */
    static get(router,params,successCallback,failureCallback){
        var requestUrl = url
        if (params) {
            let paramsArray = [];
            //拼接参数
            Object.keys(params).forEach(key => paramsArray.push(key + '=' + params[key]))
            // for (let [key, value] of params) {
            //   paramsArray.push(key + '=' + value)
            // }
            if (url.search(/\?/) === -1) {
                requestUrl += router + '?' + paramsArray.join('&')
            } else {
                requestUrl += + '&' + paramsArray.join('&')
            }
        }
        //fetch请求
        console.log(requestUrl)
        fetch(requestUrl,{
            method: 'GET',
        }).then((response) => {
            return response.json()
        }).then((data)=>{
            successCallback(data)
        }).catch((err)=>{
            failureCallback(err)
        }).done();
    }

    /********************短信平台接口********************/
    static receivePasswordMessage(mobilePhone, secCode,successCallback) {
      var content = "【智由控】尊敬的："+ mobilePhone + "，您更改密码的验证码为：" + secCode + "，请在5分钟内完成修改。#@#" + mobilePhone
      var params = {"name":name,"pwd":pwd,"content":content,"stime":stime,"sign":sign,"type":type,"extno":extno}
      let paramsArray = [];
      //拼接参数
      Object.keys(params).forEach(key => paramsArray.push(key + '=' + params[key]))
      var body = '' + paramsArray.join('&')
      fetch(messageUrl,{
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: body
      }).then((response) => {
          return response.json()
      }).then((data)=>{
          successCallback(data)
      }).catch((error) => {    // 错误处理
        console.log('*****'+error)
      }).done();
    }
}
