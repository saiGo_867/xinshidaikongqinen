import React, { Component } from 'react';
import {View,Text,LayoutAnimation,Animated,Easing} from 'react-native'
import commonVariable from '../commonVariable'
export default class MyHud extends Component{
    constructor(props){
      super(props)
      this.state = {
        isShow: false,
        info: '',
        fadeOutOpacity: new Animated.Value(0),
      }
      this.props.clickOn = null
    }

    showWithInfo(info){
      this.setState({ info:info,isShow:true },()=>{
          Animated.timing(this.state.fadeOutOpacity, {
              toValue: 1,
              duration: 500,
              easing: Easing.linear,// 线性的渐变函数
          }).start();
      })
    }

    dismiss(){
      Animated.timing(this.state.fadeOutOpacity, {
          toValue: 0,
          duration: 500,
          easing: Easing.linear,// 线性的渐变函数
      }).start(()=>{
          this.setState({isShow:false},()=>{
              if (this.props.clickOn !== null && this.props.clickOn !== undefined) {
                  this.props.clickOn()
              }
          })
      });
    }

    setUI(){
      if (this.state.isShow === false) {
          return null
      }else{
          return(
              <Animated.View // 可选的基本组件类型: Image, Text, View(可以包裹任意子View)
                  style = {{flex: 1,alignItems: 'center',justifyContent: 'center',
                            top:0,left:0,bottom:0,right:0,position:'absolute',opacity: this.state.fadeOutOpacity}}>
                          <Text style={{backgroundColor:'#FFF',textAlign:'center',width:ScreenWidth-40,
                                        paddingTop:15,paddingBottom:15,color:'#828891',fontSize:20}}>{this.state.info}</Text>
                          <View style={{backgroundColor:'#888',opacity:0.5,width:ScreenWidth-40,height:0.5}}/>
                          <Text style={{backgroundColor:'#FFF',textAlign:'center',width:ScreenWidth-40,borderTopWidth:0.5,
                                        paddingTop:10,paddingBottom:10,color:'#377AF1',fontSize:20}} onPress={()=>this.dismiss()}>确定</Text>
              </Animated.View >
          )
      }
    }

    render(){
      return this.setUI()
    }
}
MyHud.propTypes = {
    clickOn:React.PropTypes.func
}
