
import React from 'react';
let instance = null;
export default class ServerData {
  constructor(){
     if (!instance) {
       instance = this;
     }
     return instance;
  }
 /*
  * 类方法
  */
  static ShareInstance(){
    let singleton = new ServerData();
    return singleton;
  }
}

ServerData.propTypes = {
  userMessage: React.PropTypes.object.isRequire
}
