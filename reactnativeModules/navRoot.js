import React, {Component} from 'react'
import { Image } from 'react-native';
import { StackNavigator } from 'react-navigation'
import { TabNavigator } from 'react-navigation'
import Login from './Login/Login'
import Register from './Register/Register'
import ChangePassword from './ChangePassword/ChangePassword'
import Control from './Main/Control'
import UserCenter from './Main/UserCenter'
import UserMessageEdit from './Main/UserMessageEdit'
import commonVariable from './commonVariable'
// const UserStack = StackNavigator({
//     UserCenter:{ screen : UserCenter },
//     UserMessageEdit:{ screen : UserMessageEdit }
// })

const Main = TabNavigator({

    Control:{ screen : Control, navigationOptions:{
      tabBarLabel: '设备中心',
      tabBarIcon: ({tintColor,focused}) => (
          focused
              ?
              <Image
                  source={require('./img/5热水器主界面之关机状态/设备中心图标.png')}
              />
              :
              <Image
                  source={require('./img/9个人中心/设备中心.png')}
              />
      ),
      headerTitle:'空气能热水器',
      headerTitleStyle:{color:textColor,fontSize:commonVariable.scaleHorizon(55)},
      }
    },
    UserCenter:{ screen : UserCenter, navigationOptions:{
      tabBarLabel: '个人中心',
      tabBarIcon: ({tintColor,focused}) => (
            focused
                ?
                <Image
                    source={require('./img/9个人中心/选中个人中心.png')}
                />
                :
                <Image
                    source={require('./img/5热水器主界面之关机状态/个人中心图标.png')}
                />
        ),
      }
    },
  },{
        //tabBar属性
        tabBarPosition: 'bottom',
        animationEnabled:false,
        tabBarPosition: 'bottom', // 显示在底端，android 默认是显示在页面顶端的
        swipeEnabled: false, // 禁止左右滑动
        tabBarOptions: {
            activeTintColor: textColor,
            inactiveTintColor: textColor,
            activeBackgroundColor:'#3A4457FF',
            inactiveBackgroundColor:themeBackgroundColor,
            style:{backgroundColor:themeBackgroundColor,borderTopWidth:0.5,borderTopColor:inputBorderColor}
        }
  })

const Root = StackNavigator({
    Login:{ screen : Login },
    Register:{ screen : Register },
    ChangePassword:{ screen : ChangePassword },
    Main:{ screen : Main },
    UserMessageEdit:{ screen : UserMessageEdit }
},{
    initialRouteName:'Login',
    navigationOptions:{
        headerStyle:{backgroundColor:themeBackgroundColor},
        headerTitleStyle:{color:textColor,fontSize:commonVariable.scaleHorizon(55)},
    }
})

export default Root
