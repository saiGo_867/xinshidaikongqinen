//
//  NetManager.swift
//  MQTTDemo
//
//  Created by 广东壹环科技 on 16/12/26.
//  Copyright © 2016年 王立诚. All rights reserved.
//

import UIKit
import Alamofire
@objc(NetManager)
class NetManager: NSObject {
    enum Router: URLRequestConvertible {
        static let baseUrl = "http://www.yihuan100.com:82/api"
        case login(String, String)
        case regist(String, String, String)
        case getMyProduct(String)
        case getMyMate(String)
        case activaProduct(String, String)
        case changeMateUid(String, String)
        case logCmd(String, String, String, String)
        case forgetPassword(String)
        case updatePassword(String, String)
        case getMqttIP()
        case updateUserInfo(String,String,String,String,String)

        //集成维修接口
        case getProdBySn(String)
        case removeActive(String)
        case updateTDS(String,String)
        case getMateModelName()
        case updateProdName(String,String)
        case addUserMate(String,String,String,String,String,String)
        case delMate(String)
        case clearMateDataSum(String)
        case updateMateAll(String,String,String,String,String,String)
        func asURLRequest() throws -> URLRequest {
            let (path,parameters) : (String,[String:AnyObject]?) = {
                switch (self){
                case .login(let upass,let umobile):
                    let parameters = ["upass":upass,"umobile":umobile]
                    return ("MobileLogin",parameters as [String : AnyObject]?)
                case .regist(let uname,let upass, let umobile):
                    let parameters = ["uname":uname,"upass":upass,"umobile":umobile]
                    return ("Regist",parameters as [String : AnyObject]?)
                case .getMyProduct(let uid):
                    let parameters = ["uid" : uid]
                    return ("GetMyProduct",parameters as [String : AnyObject]?)
                case .getMyMate(let pid):
                    let parameters = ["pid" : pid]
                    return ("GetMyMate", parameters as [String : AnyObject]?)
                case .activaProduct(let uid, let sn):
                    let parameters = ["uid":uid, "sn":sn]
                    return("ActivaProduct", parameters as [String : AnyObject]?)
                case .changeMateUid(let uid,let sn):
                    let parameters = ["uid" : uid,"sn" : sn]
                    return ("changeMateUid",parameters as [String : AnyObject]?)
                case .logCmd(let uid, let events, let content, let uip):
                    let parameters = ["uid": uid, "events":events,"content":content,"uip":uip]
                    return("LogCmd",parameters as [String : AnyObject]?)
                case .forgetPassword(let u_word):
                    let parameters = ["u_word": u_word]
                    return("ForgetPassword", parameters as [String : AnyObject]?)
                case .updatePassword(let u_pass, let u_mobile):
                    let parameters = ["u_pass":u_pass, "u_mobile":u_mobile]
                    return("UpdatePassword", parameters as [String : AnyObject]?)
                case .getMqttIP():
                    return("GetMqttIP",[:])
                
                //集成维修的接口
                case .getProdBySn(let psn):
                    let parameters = ["psn" : psn]
                    return ("GetProdBySn",parameters as [String : AnyObject]?)
                case .removeActive(let sn):
                    let parameters  = ["sn" : sn]
                    return ("RemoveActive", parameters as [String : AnyObject]?)
                case .updateTDS(let psn, let tds):
                    let parameters = ["psn" : psn, "tds" : tds]
                    return ("UpdateTDS", parameters as [String : AnyObject]?)
                case .getMateModelName():
                    return("GetMateModelName",[:])
                case .updateProdName(let pid, let pname):
                    let parameters = ["pid":pid,"pname":pname]
                    return("UpdateProdName", parameters as [String : AnyObject]?)
                case .addUserMate(let uid, let pid, let matenum, let matename, let matelimit, let mateendtime):
                    let parameters = ["uid":uid, "pid":pid, "matenum":matenum, "matename":matename, "matelimit":matelimit ,"mateendtime":mateendtime]
                    return("AddUserMate",parameters as [String : AnyObject]?)
                case .delMate(let mid):
                    let parameters = ["mid":mid]
                    return("DelMate",parameters as [String : AnyObject]?)
                case .clearMateDataSum(let mid):
                    let parameters = ["mid" : mid]
                    return ("ClearMateDataSum", parameters as [String : AnyObject]?)
                case .updateMateAll(let mid, let matenum, let matename, let matelimit, let matestarttime, let mateendtime):
                    let parameters = ["mid":mid, "matenum":matenum, "matename":matename, "matelimit":matelimit, "matestarttime":matestarttime, "mateendtime":mateendtime]
                    return("UpdateMateAll", parameters as [String : AnyObject]?)
                case .updateUserInfo(let uid, let realname, let address, let sex, let mail):
                    let parameters = ["uid":uid,"realname":realname,"address":address,"sex":sex,"mail":mail]
                    return("UpDateUserInfo", parameters as [String : AnyObject]?)
               }
            }()
            let url = URL(string: Router.baseUrl)
            let urlrequest = URLRequest(url: url!.appendingPathComponent(path))
            return try URLEncoding.default.encode(urlrequest, with: parameters)
        }
    }
    
    private class func requestCmd(request:URLRequestConvertible, completion:@escaping (_ response: DataResponse<Any>)-> Void){
        var urlRequest : URLRequest!
        do {
            urlRequest = try request.asURLRequest()
            urlRequest.timeoutInterval = 10
        } catch{}
        Alamofire.request(urlRequest).responseJSON { (response) in
            completion(response)
        }
    }
    
    /**登录**/
    class func login(_ upass:String, umobile:String,completion:@escaping (_ response:DataResponse<Any>)->Void) {
        requestCmd(request: Router.login(upass, umobile)) { (response) in
            completion(response)
        }
    }
    
    /**注册**/
    class func regist(_ uname: String, upass: String, umobile: String, completion:@escaping (_ response: DataResponse<Any>)->Void){
        requestCmd(request: Router.regist(uname, upass, umobile)) { (response) in
            completion(response)
        }
    }
    
    /**获取用产品**/
    class func getMyProduct(_ uid: String,completion:@escaping (_ response: DataResponse<Any>)->Void){
        requestCmd(request: Router.getMyProduct(uid)) { (response) in
            completion(response)
        }
    }
    
    /**获取水龙头产品下的滤芯**/
    class func getMyMate(_ pid: String, completion:@escaping (_ response: DataResponse<Any>)->Void){
        requestCmd(request: Router.getMyMate(pid)) { (response) in
            completion(response)
        }
    }
    
    /**激活产品**/
    class func activaProduct(_ uid: String, sn: String, completion:@escaping (_ response: DataResponse<Any>)->Void){
        requestCmd(request: Router.activaProduct(uid, sn)) { (response) in
            completion(response)
        }
    }
    
    /**激活产品后调用**/
    class func changeMateUid(_ uid: String, sn: String, completion:@escaping (_ response: DataResponse<Any>)->Void){
        requestCmd(request: Router.changeMateUid(uid, sn)) { (response) in
            completion(response)
        }
    }
    
    /**日志记录接口**/
    class func logCmd(_ uid: String, events: String, content: String, uip: String, completion:@escaping (_ response: DataResponse<Any>)->Void){
        requestCmd(request: Router.logCmd(uid, events, content, uip)) { (response) in
            completion(response)
        }
    }
    
    /**忘记密码(获取旧密码)**/
    class func forgetPassword(_ u_word:String, completion:@escaping (_ response: DataResponse<Any>)->Void) {
        requestCmd(request: Router.forgetPassword(u_word)) { (response) in
            completion(response)
        }
    }
    
    /**修改密码**/
    class func updatePassword(_ u_pass:String, u_mobile:String ,completion:@escaping (_ response: DataResponse<Any>)->Void) {
        requestCmd(request: Router.updatePassword(u_pass, u_mobile)) { (response) in
            completion(response)
        }
    }
    
    /**获取MQTT地址与账号密码**/
    class func getMqttIP(completion:@escaping (_ response: DataResponse<Any>)->Void) {
        requestCmd(request: Router.getMqttIP()) { (response) in
            completion(response)
        }
    }

/************************维修的接口*****************************/
    /**通过sn号获取产品所有的信息*/
    class func getProdBySn(_ psn:String, completion:@escaping(_ response: DataResponse<Any>)->Void){
        requestCmd(request: Router.getProdBySn(psn)) { (response) in
            completion(response)
        }
    }
    
    /**解绑产品*/
    class func removeActive(_ sn:String, completion: @escaping (_ resopnse:DataResponse<Any>)->Void){
        requestCmd(request: Router.removeActive(sn)) { (response) in
            completion(response)
        }
    }
    
    /**设置水龙头初始TDS*/
    class func UpdateTDS(_ psn:String, tds: String, completion: @escaping (_ resopnse:DataResponse<Any>)->Void){
        requestCmd(request: Router.updateTDS(psn, tds)) { (response) in
            completion(response)
        }
    }
    
    /**获取滤芯名称**/
    class func getMateModelName(completion:@escaping (_ response: DataResponse<Any>)->Void) {
        requestCmd(request: Router.getMateModelName()) { (response) in
            completion(response)
        }
    }
    
    /**更改产品名称型号**/
    class func updateProdName(_ pid:String, pname: String, completion: @escaping (_ resopnse:DataResponse<Any>)->Void){
        requestCmd(request: Router.updateProdName(pid, pname)) { (response) in
            completion(response)
        }
    }
    
    /**添加滤芯(new)**/
    class func addUserMate(_ uid:String, pid:String, matenum:String, matename:String, matelimit:String, mateendtime:String ,completion:@escaping (_ response: DataResponse<Any>)->Void) {
        requestCmd(request: Router.addUserMate(uid, pid, matenum, matename, matelimit, mateendtime)) { (response) in
            completion(response)
        }
    }
    
    /**删除滤芯API**/
    class func delMate(_ mid:String ,completion:@escaping (_ response: DataResponse<Any>)->Void) {
        requestCmd(request: Router.delMate(mid)) { (response) in
            completion(response)
        }
    }

    /**清除滤芯流量且更新滤芯状态*/
    class func clearMateDataSum(_ mid:String, completion: @escaping (_ resopnse:DataResponse<Any>)->Void){
        requestCmd(request: Router.clearMateDataSum(mid)) { (response) in
            completion(response)
        }
    }
    
    /**更新全部滤芯**/
    class func updateMateAll(_ mid:String, matenum:String, matename:String, matelimit:String, matestarttime:String, mateendtime:String, completion:@escaping (_ response: DataResponse<Any>)->Void) {
        requestCmd(request: Router.updateMateAll(mid, matenum, matename, matelimit, matestarttime, mateendtime)) { (response) in
            completion(response)
        }
    }
    
    /**修改用户资料API**/
    class func upDateUserInfo(_ uid:String, realname:String, address:String, sex:String, mail:String,completion:@escaping (_ response: DataResponse<Any>)->Void) {
        
        requestCmd(request: Router.updateUserInfo(uid, realname, address, sex, mail)) { (response) in
            completion(response)
        }
    }
}
    
/************************暂时用不上*****************************/
/**
    //获取本机ip地址
    class func getIFAddresses() -> [String] {
        var addresses = [String]()
        
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return [] }
        guard let firstAddr = ifaddr else { return [] }
        
        // For each interface ...
        for ptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let flags = Int32(ptr.pointee.ifa_flags)
            var addr = ptr.pointee.ifa_addr.pointee
            
            // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
            if (flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING) {
                if addr.sa_family == UInt8(AF_INET) || addr.sa_family == UInt8(AF_INET6) {
                    
                    // Convert interface address to a human readable string:
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    if (getnameinfo(&addr, socklen_t(addr.sa_len), &hostname, socklen_t(hostname.count),
                                    nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                        let address = String(cString: hostname)
                        addresses.append(address)
                    }
                }
            }
        }
        freeifaddrs(ifaddr)
        return addresses
    }
    
    //获取当前网速
    class func getInterfaceBytes()-> CLongLong {
        var ifa_list : UnsafeMutablePointer<ifaddrs>? = UnsafeMutablePointer<ifaddrs>(bitPattern: 0)
        var ifa : UnsafeMutablePointer<ifaddrs>?
        if getifaddrs(&ifa_list) == -1 {
            return 0
        }
        var iBytes : uint = 0
        var oBytes : uint = 0
        ifa = ifa_list
        for ptr in sequence(first: ifa, next: { $0?.pointee.ifa_next }) {
            if AF_LINK != Int32(ptr!.pointee.ifa_addr.pointee.sa_family) {
                continue
            }
            let ifa_flags = ptr!.pointee.ifa_flags
            let iffUp = uint(IFF_UP)
            let iffRunning = uint(IFF_RUNNING)
            if ((ifa_flags & iffUp) == 0 && (ifa_flags & iffRunning) == 0) {
                continue
            }
            
            if ptr!.pointee.ifa_data == nil {
                continue
            }
            
            if strncmp(ptr?.pointee.ifa_name, "lo", 2) > 0 {
                let data = unsafeBitCast(ptr!.pointee.ifa_data, to: UnsafeMutablePointer<if_data>.self)
                iBytes += data.pointee.ifi_ibytes
                oBytes += data.pointee.ifi_obytes
                print(iBytes, oBytes)
            }
        }
        return CLongLong(iBytes + oBytes)
    }
}

struct DataUsageInfo {
    var wifiReceived: UInt32 = 0
    var wifiSent: UInt32 = 0
    var wirelessWanDataReceived: UInt32 = 0
    var wirelessWanDataSent: UInt32 = 0
    
    mutating func updateInfoByAdding(info: DataUsageInfo) {
        wifiSent += info.wifiSent
        wifiReceived += info.wifiReceived
        wirelessWanDataSent += info.wirelessWanDataSent
        wirelessWanDataReceived += info.wirelessWanDataReceived
    }
}

class DataUsage {
    
    private static let wwanInterfacePrefix = "pdp_ip"
    private static let wifiInterfacePrefix = "en"
    
    class func getDataUsage() -> DataUsageInfo {
        var interfaceAddresses: UnsafeMutablePointer<ifaddrs>? = nil
        var dataUsageInfo = DataUsageInfo()
        
        guard getifaddrs(&interfaceAddresses) == 0 else { return dataUsageInfo }
        
        var pointer = interfaceAddresses
        while pointer != nil {
            guard let info = getDataUsageInfo(from: pointer!) else {
                pointer = pointer?.pointee.ifa_next
                continue
            }
            dataUsageInfo.updateInfoByAdding(info: info)
            pointer = pointer?.pointee.ifa_next
        }
        
        freeifaddrs(interfaceAddresses)
        
        return dataUsageInfo
    }
    
    private class func getDataUsageInfo(from infoPointer: UnsafeMutablePointer<ifaddrs>) -> DataUsageInfo? {
        let pointer = infoPointer
        
        let name: String! = String.init(describing: infoPointer.pointee.ifa_name)
        
        let addr = pointer.pointee.ifa_addr.pointee
        guard addr.sa_family == UInt8(AF_LINK) else { return nil }
        
        return dataUsageInfo(from: pointer, name: name)
    }
    
    private class func dataUsageInfo(from pointer: UnsafeMutablePointer<ifaddrs>, name: String) -> DataUsageInfo {
        var networkData: UnsafeMutablePointer<if_data>? = nil
        var dataUsageInfo = DataUsageInfo()
        
        if name.hasPrefix(wifiInterfacePrefix) {
            networkData = unsafeBitCast(pointer.pointee.ifa_data, to: UnsafeMutablePointer<if_data>.self)
            dataUsageInfo.wifiSent += (networkData?.pointee.ifi_obytes)!
            dataUsageInfo.wifiReceived += (networkData?.pointee.ifi_ibytes)!
        } else if name.hasPrefix(wwanInterfacePrefix) {
            networkData = unsafeBitCast(pointer.pointee.ifa_data, to: UnsafeMutablePointer<if_data>.self)
            dataUsageInfo.wirelessWanDataSent += (networkData?.pointee.ifi_obytes)!
            dataUsageInfo.wirelessWanDataReceived += (networkData?.pointee.ifi_ibytes)!
        }
        
        return dataUsageInfo
    }
}
**/
