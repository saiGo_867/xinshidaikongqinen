//
//  RNHelper.m
//  新时代空气能热水器
//
//  Created by 广东壹环科技 on 2017/6/27.
//  Copyright © 2017年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
@interface RCT_EXTERN_MODULE(RNHelper, NSObject) //RCT_EXTERN_MODULE将模块导出到Reac-Native
RCT_EXTERN_METHOD(openAirWaterHeater:(NSString *)sn) //RCT_EXTERN_METHOD将方法导出到ReacNative
RCT_EXTERN_METHOD(closeAirWaterHeater:(NSString *)sn)
RCT_EXTERN_METHOD(addTemperatureAirHeater:(NSString *)sn)
RCT_EXTERN_METHOD(subTemperatureAirHeater:(NSString *)sn)
RCT_EXTERN_METHOD(forceCutOffAirHeater:(NSString *)sn)
RCT_EXTERN_METHOD(forceCutOffAirHeater:(NSString *)sn)
RCT_EXTERN_METHOD(recoverNormalAirHeater:(NSString *)sn)
RCT_EXTERN_METHOD(mqttObserveMessage:(NSString *)sn)
RCT_EXTERN_METHOD(mqttSubscribe:(NSString *)sn)
@end

