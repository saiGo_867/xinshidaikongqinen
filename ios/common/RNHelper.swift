//
//  RNHelper.swift
//  新时代空气能热水器
//
//  Created by 广东壹环科技 on 2017/6/27.
//  Copyright © 2017年 Facebook. All rights reserved.
//

import UIKit
import RxSwift
@objc(RNHelper)
class RNHelper: NSObject,RCTBridgeModule {
  
  let disposeBag = DisposeBag()
  static func moduleName() -> String! {
    return "RNHelper"
  }
  var bridge: RCTBridge!  
  @objc(openAirWaterHeater:)
  func openAirWaterHeater(sn:String){
    MQTTManager.shareMQTTManager.openAirHeater(sn)
  }
  
  @objc(closeAirWaterHeater:)
  func closeAirWaterHeater(sn:String){
      MQTTManager.shareMQTTManager.closeAirHeater(sn)
  }
  
  @objc(addTemperatureAirHeater:)
  func addTemperatureAirHeater(sn:String){
      MQTTManager.shareMQTTManager.addTemperatureAirHeater(sn)
  }
  
  @objc(subTemperatureAirHeater:)
  func subTemperatureAirHeater(sn:String){
      MQTTManager.shareMQTTManager.subTemperatureAirHeater(sn)
  }
  
  @objc(forceCutOffAirHeater:)
  func forceCutOffAirHeater(sn:String){
      MQTTManager.shareMQTTManager.forceCutOffAirHeater(sn)
  }
  
  @objc(recoverNormalAirHeater:)
  func recoverNormalAirHeater(sn:String){
    MQTTManager.shareMQTTManager.recoverNormalAirHeater(sn)
  }
  
  @objc(mqttObserveMessage:)
  func mqttObserveMessage(sn:String){
      (UIApplication.shared.delegate as! AppDelegate).SN = sn
      MQTTManager.shareMQTTManager.mqtt.subscribe(subscibeTopic+sn, qos: .qos1)
      MQTTManager.shareMQTTManager.mqttDelegate.messageEvent.asObservable().shareReplay(1).subscribe({ [unowned self] (event) in
        guard event.element != nil else {
          return
        }
        self.bridge.eventDispatcher().sendAppEvent(withName: "MQTTMessage", body: ["MQTTMessage":event.element!])
      }).addDisposableTo(disposeBag)
  }
  
  @objc(mqttSubscribe:)
  func mqttSubscribe(sn:String){
    if (UIApplication.shared.delegate as! AppDelegate).SN != nil {
        MQTTManager.shareMQTTManager.mqtt.unsubscribe(subscibeTopic + (UIApplication.shared.delegate as! AppDelegate).SN!)
    }
    (UIApplication.shared.delegate as! AppDelegate).SN = sn
    MQTTManager.shareMQTTManager.mqtt.subscribe(subscibeTopic+sn, qos: .qos1)
  }
}
