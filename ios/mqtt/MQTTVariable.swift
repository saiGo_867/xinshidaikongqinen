public let subscibeTopic :String = "yh/prod/class/kqn/s/"
public let publishTopic :String = "yh/prod/class/kqn/p/"
public var mqttHost :String = "www.difiot.com"
public var mqttPort :UInt16 = 1883
public var mqttUserName : String = "347242@gmail.com"
public var mqttPassword : String = "lilian780304"

/**
 MQTT发布消息枚举
 ***/
//水龙头命令
enum mqttTapPublishCmd : String {
    /**打开指令*/
    case openCmd = "cmdop"
    
    /**关闭指令*/
    case closeCmd = "cmdcl"
    
    /**清除原wifi指令(不能真正清除重新上电自动重连)*/
    case wifiClearCmd = "cmdsm"
    
    /**清除原wifi指令(真正清除)*/
    case wifiRemoveCmd = "cmdrm"
    
    /**设置TDS值指令*/
    case setTDSCmd = "cmdcs"
    
    /**流量清0指令*/
    case clearFlwCmd = "cmdcr"
    
    /**模式切换指令*/
    case patternChangeCmd = "cmdch"
    
    /**解锁水龙头指令*/
    case unlockCmd = "lx"
}

//空气净化器命令
enum mqttAirCleanerPublishCmd : String {
    /**开关指令*/
    case openAndCloseCmd = "1"
    
    /**风速指令*/
    case windSpeedCmd = "2"
    
    /**定时指令*/
    case timingCmd = "3"
    
    /**自动*/
    case autoCmd = "4"
    
    /**睡眠*/
    case sleepCmd = "5"
    
    func controlCmd(_ sn:String) -> String{
        let commonComponent = "cmdyk"
        switch self {
        case .openAndCloseCmd:
            return commonComponent + sn + self.rawValue
        case .windSpeedCmd:
            return commonComponent + sn + self.rawValue
        case .timingCmd:
            return commonComponent + sn + self.rawValue
        case .autoCmd:
            return commonComponent + sn + self.rawValue
        case .sleepCmd:
            return commonComponent + sn + self.rawValue
        }
    }
}

//空气能热水器命令
enum mqttAirWaterHeaterPublishCmd : String {
  
    /**开指令*/
    case openCmd = "op"

    /**关指令*/
    case closeCmd = "cl"
  
    /**温度+指令*/
    case temperatureAdd = "st+"
    
    /**温度-指令*/
    case temperatureSub = "st-"
  
    /**到期强制断开*/
    case forceCutOff = "lx1"
  
    /**恢复正常使用*/
    case recoverNormal = "lx0"
  
    func controlCmd(_ sn:String) -> String{
        let commonComponent = "cmd"
        switch self {
        case .openCmd:
            return commonComponent + self.rawValue + sn
        case .closeCmd:
            return commonComponent + self.rawValue + sn
        case .temperatureAdd:
            return commonComponent + self.rawValue.replacingOccurrences(of: "+", with: "") + sn + "+"
        case .temperatureSub:
            return commonComponent + self.rawValue.replacingOccurrences(of: "-", with: "") + sn + "-"
        case .forceCutOff:
            return commonComponent + self.rawValue.replacingOccurrences(of: "1", with: "") + sn + "1"
        case .recoverNormal:
            return commonComponent + self.rawValue.replacingOccurrences(of: "", with: "0") + sn + "0"
        }
    }
}

/**
 接受到的MQTT消息枚举
 ***/
//水龙头
enum mqttTapReceiveMessage : String{
    case open = "REop"
    case close = "REcl"
    case tds = "tds"
    case flw = "flw"
    case fla = "fla"
    case ofl = "ofl"
}

//空气净化器
enum mqttAirCleanerReceiveMessage : String{
    case close = "REcl"
    case open = "RE"
    case flw = "flw"
}

//空气能热水器
enum mqttAirWaterHeaterReceiveMessage : String{
    case flw = "flw"
    case tem = "tem"
}

