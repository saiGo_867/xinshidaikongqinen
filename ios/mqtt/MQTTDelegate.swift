//
//  MQTTDelegate.swift
//  MQTTDemo
//
//  Created by 广东壹环科技 on 16/12/26.
//  Copyright © 2016年 王立诚. All rights reserved.
//

import UIKit
import CocoaMQTT
import RxSwift
import RxCocoa
class MQTTDelegate: NSObject,CocoaMQTTDelegate {
    
    var messageEvent = Variable<String>("")
    
    func mqtt(_ mqtt: CocoaMQTT, didConnect host: String, port: Int) {
        if let sn = (UIApplication.shared.delegate as! AppDelegate).SN {
            mqtt.subscribe(subscibeTopic+sn, qos: .qos1)
        }
    }
    
    func mqttDidDisconnect(_ mqtt: CocoaMQTT, withError err: Error?) {
        
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didConnectAck ack: CocoaMQTTConnAck) {
        
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didPublishMessage message: CocoaMQTTMessage, id: UInt16) {
        
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didPublishAck id: UInt16) {
        
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didPublishComplete id: UInt16) {
        
    }
    
    
    func mqtt(_ mqtt: CocoaMQTT, didReceiveMessage message: CocoaMQTTMessage, id: UInt16) {
        messageEvent.value = message.string ?? ""
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didReceive trust: SecTrust, completionHandler: @escaping (Bool) -> Void) {
        
    }
    
    
    func mqtt(_ mqtt: CocoaMQTT, didSubscribeTopic topic: String) {
        
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didUnsubscribeTopic topic: String) {
        
    }
    
    func mqttDidReceivePong(_ mqtt: CocoaMQTT) {
        
    }
    
    func mqttDidPing(_ mqtt: CocoaMQTT) {
        
    }
}
