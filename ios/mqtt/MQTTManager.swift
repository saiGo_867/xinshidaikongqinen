//
//  MQTTManager.swift
//  MQTTDemo
//
//  Created by 广东壹环科技 on 16/12/26.
//  Copyright © 2016年 王立诚. All rights reserved.
//

import UIKit
import CocoaMQTT

final class MQTTManager {
    
    static let shareMQTTManager : MQTTManager = MQTTManager()
    
    private var timer : Timer!
    var mqtt: CocoaMQTT!
    
    var mqttDelegate : MQTTDelegate!

    private init() {
        let clientID = "CocoaMQTT-" + String(ProcessInfo().processIdentifier)
        mqtt = CocoaMQTT(clientID: clientID, host: mqttHost, port: mqttPort)
        mqtt.username = mqttUserName
        mqtt.password = mqttPassword
        mqtt.keepAlive = 60
        mqttDelegate = MQTTDelegate()
        mqtt.delegate = mqttDelegate
        mqttConnect()
    }
    
    public final func mqttConnect(){
        mqtt.connect()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(MQTTManager.monitorConnect), userInfo: nil, repeats: true)
        RunLoop.current.add(timer, forMode: RunLoopMode.commonModes)
    }
    
    @objc private func monitorConnect(){
        if mqtt.connState == .disconnected {
            NetManager.getMqttIP(completion: { [unowned self] (response) in
                let ipString = String.init(data: response.data!, encoding: String.Encoding.utf8)
                let StringArr = ipString?.components(separatedBy: ":")
                for i in 0..<StringArr!.count {
                    switch i {
                        case 0:
                        mqttHost = StringArr![i]
                        self.mqtt.host = mqttHost
                        case 1:
                        mqttPort = UInt16(StringArr![i])!
                        self.mqtt.port = mqttPort
                        case 2:
                        mqttUserName = StringArr![i]
                        self.mqtt.username = mqttUserName
                        case 3:
                        mqttPassword = StringArr![i]
                        self.mqtt.password = mqttPassword
                        default: break
                    }
                }
            })
            mqtt.connect()
        }
    }
    
    public final func subscriptTopic(_ topic : String){
        mqtt.subscribe(topic, qos: .qos0)
    }
    
    public final func closeConnect(){
        timer.invalidate()
        mqtt.disconnect()
    }
    
    //MARK:水龙头控制
    public final func openTap(_ sn:String){
        mqtt.publish(CocoaMQTTMessage.init(topic: "\(publishTopic)\(sn)", string: "\(mqttTapPublishCmd.openCmd.rawValue)\(sn)"))
    }
    
    public final func closeTap(_ sn:String){
        mqtt.publish(CocoaMQTTMessage.init(topic: "\(publishTopic)\(sn)", string: "\(mqttTapPublishCmd.closeCmd.rawValue)\(sn)"))
    }
    
    public final func clearWifi(_ sn: String){
        mqtt.publish(CocoaMQTTMessage.init(topic: "\(publishTopic)\(sn)", string: "\(mqttTapPublishCmd.wifiClearCmd.rawValue)\(sn)"))
    }
    
    public final func removeWifi(_ sn: String){
        mqtt.publish(CocoaMQTTMessage.init(topic: "\(publishTopic)\(sn)", string: "\(mqttTapPublishCmd.wifiRemoveCmd.rawValue)\(sn)"))
    }
    
    public final func setTDS(_ sn: String, tds: CGFloat){
        mqtt.publish(CocoaMQTTMessage.init(topic: "\(publishTopic)\(sn)", string: "\(mqttTapPublishCmd.setTDSCmd.rawValue)\(sn)\(tds)"))
    }
    
    public final func clearFlw(_ sn: String){
        mqtt.publish(CocoaMQTTMessage.init(topic: "\(publishTopic)\(sn)", string: "\(mqttTapPublishCmd.clearFlwCmd.rawValue)\(sn)"))
    }
    
    public final func changePattern(_ sn: String){
        mqtt.publish(CocoaMQTTMessage.init(topic: "\(publishTopic)\(sn)", string: "\(mqttTapPublishCmd.patternChangeCmd)\(sn)"))
    }
    
    /**解锁水龙头*/
    public final func unlockTap(sn:String) {
        mqtt.publish(CocoaMQTTMessage.init(topic: "\(publishTopic)\(sn)", string: mqttTapPublishCmd.unlockCmd.rawValue + sn + "0"))
    }
    
    //MARK:空气净化器控制
    public final func openCloseAirCleaner(_ sn: String){
        mqtt.publish(CocoaMQTTMessage.init(topic: "\(publishTopic)\(sn)", string: mqttAirCleanerPublishCmd.openAndCloseCmd.controlCmd(sn)))
    }
    
    public final func windSpeedControl(_ sn: String){
        mqtt.publish(CocoaMQTTMessage.init(topic: "\(publishTopic)\(sn)", string: mqttAirCleanerPublishCmd.windSpeedCmd.controlCmd(sn)))
    }
    
    public final func autoControl(_ sn: String){
        mqtt.publish(CocoaMQTTMessage.init(topic: "\(publishTopic)\(sn)", string: mqttAirCleanerPublishCmd.autoCmd.controlCmd(sn)))
    }
    
    public final func sleepControl(_ sn: String){
        mqtt.publish(CocoaMQTTMessage.init(topic: "\(publishTopic)\(sn)", string: mqttAirCleanerPublishCmd.sleepCmd.controlCmd(sn)))
    }
    
    public final func timingControl(_ sn: String){
        mqtt.publish(CocoaMQTTMessage.init(topic: "\(publishTopic)\(sn)", string: mqttAirCleanerPublishCmd.timingCmd.controlCmd(sn)))
    }
    
    //MARK:空气能热水器控制
    public final func openAirHeater(_ sn: String){
        mqtt.publish(CocoaMQTTMessage.init(topic: "\(publishTopic)\(sn)", string: mqttAirWaterHeaterPublishCmd.openCmd.controlCmd(sn)))
    }
  
    public final func closeAirHeater(_ sn: String){
        mqtt.publish(CocoaMQTTMessage.init(topic: "\(publishTopic)\(sn)", string: mqttAirWaterHeaterPublishCmd.closeCmd.controlCmd(sn)))
    }
    
    public final func addTemperatureAirHeater(_ sn:String){
        mqtt.publish(CocoaMQTTMessage.init(topic: "\(publishTopic)\(sn)", string: mqttAirWaterHeaterPublishCmd.temperatureAdd.controlCmd(sn)))
    }
    
    public final func subTemperatureAirHeater(_ sn:String){
        mqtt.publish(CocoaMQTTMessage.init(topic: "\(publishTopic)\(sn)", string: mqttAirWaterHeaterPublishCmd.temperatureSub.controlCmd(sn)))
    }
  
    public final func forceCutOffAirHeater(_ sn:String){
        mqtt.publish(CocoaMQTTMessage.init(topic: "\(publishTopic)\(sn)", string: mqttAirWaterHeaterPublishCmd.forceCutOff.controlCmd(sn)))
    }
  
    public final func recoverNormalAirHeater(_ sn:String){
        mqtt.publish(CocoaMQTTMessage.init(topic: "\(publishTopic)\(sn)", string: mqttAirWaterHeaterPublishCmd.recoverNormal.controlCmd(sn)))
    }
}
