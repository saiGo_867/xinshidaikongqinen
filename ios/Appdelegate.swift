//
//  Appdelegate.swift
//  LearnRN
//
//  Created by 广东壹环科技 on 2017/6/14.
//  Copyright © 2017年 Facebook. All rights reserved.
//

import Foundation
import IQKeyboardManagerSwift
@UIApplicationMain
@objc(AppDelegate)
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var SN: String?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        initApp()
        let jsCodeLocation = RCTBundleURLProvider.sharedSettings().jsBundleURL(forBundleRoot: "index.ios", fallbackResource: nil)
      
        let rootView = RCTRootView(bundleURL: jsCodeLocation, moduleName: "LearnRN", initialProperties: nil, launchOptions: launchOptions)
        rootView?.backgroundColor = UIColor.white
        window = UIWindow(frame: UIScreen.main.bounds)
        let rootViewController = UIViewController()
        rootViewController.view = rootView
        window?.rootViewController = rootViewController
        window?.makeKeyAndVisible()
        return true
    }
  
    fileprivate func initApp(){
        MQTTManager.shareMQTTManager //连接MQTT
        IQKeyboardManager.sharedManager().enable = true //键盘设置
    }
}
