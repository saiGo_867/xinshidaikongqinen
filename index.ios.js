// /**
//  * Sample React Native App
//  * https://github.com/facebook/react-native
//  * @flow
//  */
//

import commonVariable from './reactnativeModules/commonVariable.js'
import { AppRegistry } from 'react-native';
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from './reactnativeModules/store';
import Entry from './reactnativeModules/Entry'
export default class App extends Component {
  render() {
    return (
        <Provider store={store}>
            <Entry/>
        </Provider>
    );
  }
}

AppRegistry.registerComponent('LearnRN', () => App);


// import React, { Component } from 'react';
// import {
//   AppRegistry,Image,View
// } from 'react-native';
// import { StackNavigator } from 'react-navigation';
// import { TabNavigator } from 'react-navigation';
// import commonVariable from './reactnativeModules/commonVariable.js'
// import Login from './reactnativeModules/Login/Login'
// import Register from './reactnativeModules/Register/Register'
// import ChangePassword from './reactnativeModules/ChangePassword/ChangePassword'
// import Control from './reactnativeModules/Main/Control'
// import UserCenter from './reactnativeModules/Main/UserCenter'
// import UserMessageEdit from './reactnativeModules/Main/UserMessageEdit'
//
// // const UserStack = StackNavigator({
// //     UserCenter:{ screen : UserCenter },
// //     UserMessageEdit:{ screen : UserMessageEdit }
// // })
//
// const Main = TabNavigator({
//
//     Control:{ screen : Control, navigationOptions:{
//       tabBarLabel: '设备中心',
//       tabBarIcon: ({tintColor,focused}) => (
//           focused
//               ?
//               <Image
//                   source={require('./reactnativeModules/img/5热水器主界面之关机状态/设备中心图标.png')}
//               />
//               :
//               <Image
//                   source={require('./reactnativeModules/img/9个人中心/设备中心.png')}
//               />
//       ),
//       headerTitle:'空气能热水器',
//       headerTitleStyle:{color:textColor,fontSize:commonVariable.scaleHorizon(55)},
//       }
//     },
//     UserCenter:{ screen : UserCenter, navigationOptions:{
//       tabBarLabel: '个人中心',
//       tabBarIcon: ({tintColor,focused}) => (
//             focused
//                 ?
//                 <Image
//                     source={require('./reactnativeModules/img/9个人中心/选中个人中心.png')}
//                 />
//                 :
//                 <Image
//                     source={require('./reactnativeModules/img/5热水器主界面之关机状态/个人中心图标.png')}
//                 />
//         ),
//       }
//     },
//   },{
//         //tabBar属性
//         tabBarPosition: 'bottom',
//         swipeEnabled:false,
//         animationEnabled:false,
//         tabBarOptions: {
//             activeTintColor: textColor,
//             inactiveTintColor: textColor,
//             activeBackgroundColor:'#3A4457FF',
//             inactiveBackgroundColor:themeBackgroundColor,
//             style:{backgroundColor:themeBackgroundColor,borderTopWidth:0.5,borderTopColor:inputBorderColor}
//         }
//   })
//
// const Root = StackNavigator({
//     Login:{ screen : Login },
//     Register:{ screen : Register },
//     ChangePassword:{ screen : ChangePassword },
//     Main:{ screen : Main },
//     UserMessageEdit:{ screen : UserMessageEdit }
// },{
//     initialRouteName:'Login',
//     navigationOptions:{
//         headerStyle:{backgroundColor:themeBackgroundColor},
//         headerTitleStyle:{color:textColor,fontSize:commonVariable.scaleHorizon(55)},
//     }
//
// })
//
//
//
// AppRegistry.registerComponent('LearnRN', () => Root);

 //import React from 'react';
 //import {
 //    AppRegistry,
 //    Text,
 //    Button,
 //    View,
 //} from 'react-native';
 //
 //import { StackNavigator } from 'react-navigation';
 //import { TabNavigator } from 'react-navigation';
 //
 //class ChatScreen extends React.Component {
 //    static navigationOptions = ({ navigation }) => ({
 //        title: `Chat with ${navigation.state.params.user}`,
 //    });
 //    render() {
 //        const { params } = this.props.navigation.state;
 //        return (
 //            <View>
 //                <Text>Chat with {params.user}</Text>
 //            </View>
 //        );
 //    }
 //}
 //
 //class RecentChatsScreen extends React.Component {
 //    render() {
 //        const { navigate } = this.props.navigation;
 //        return (
 //            <View>
 //                <Text>List of recent chats</Text>
 //                <Button
 //                    onPress={() => navigate('Chat', {user: 'Lucy'})}
 //                    title="Chat with Lucy"
 //                />
 //            </View>
 //        );
 //    }
 //}
 //
 //class AllContactsScreen extends React.Component {
 //    render() {
 //        const { navigate } = this.props.navigation;
 //        return (
 //            <View/>
 //
 //        );
 //    }
 //}
 //
 //const MainScreenNavigator = TabNavigator({
 //    Recent: { screen: RecentChatsScreen },
 //    All: { screen: AllContactsScreen },
 //},{
 //    tabBarOptions: {
 //        activeTintColor: 'white',
 //        inactiveTintColor: 'white',
 //        activeBackgroundColor:'green',
 //        style: {
 //            backgroundColor: 'blue',
 //        },
 //    },
 //});
 //
 //MainScreenNavigator.navigationOptions = {
 //    title: 'My Chats',
 //};
 //
 //const SimpleAppReactNavigation = StackNavigator({
 //    Home: { screen: MainScreenNavigator },
 //    Chat: { screen: ChatScreen },
 //});
 //AppRegistry.registerComponent('LearnRN', () => SimpleAppReactNavigation);
